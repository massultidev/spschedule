/* Copyright by: P.J. Grochowski */

#include "Course.h"

#include <Poco/Format.h>
#include <Poco/StringTokenizer.h>
#include <Poco/NumberParser.h>

namespace gdev {
namespace spSchedule {

// Save file JSON keys:
static const std::string SUBJECT 		= "subject";
static const std::string TUTOR 			= "tutor";
static const std::string PLACE 			= "place";
static const std::string COURSE_TYPE 	= "course_type";
static const std::string DAY_OF_WEEK 	= "day_of_week";
static const std::string WEEK_CYCLE 	= "week_cycle";
static const std::string TIME 			= "time";

Poco::SharedPtr < std::vector < Course::Ptr > > Course::mCourses = new std::vector < Course::Ptr >();

Course::Course(
		std::string subject,
		std::string tutor,
		std::string place,
		Settings::CourseType type,
		Settings::DayOfWeek day,
		Settings::WeekCycle weekCycle,
		int startHour,
		int startMinute,
		int endHour,
		int endMinute
):
		mSubject( subject ),
		mTutor( tutor ),
		mPlace( place ),
		mType( type ),
		mDay( day ),
		mWeekCycle( weekCycle ),
		mStartHour( startHour ),
		mStartMinute( startMinute ),
		mEndHour( endHour ),
		mEndMinute( endMinute )
{
	mSettings = Settings::getInstance();

	verifyTime( startHour, startMinute, endHour, endMinute );
}
Course::~Course() {
}

Poco::SharedPtr < std::vector < Course::Ptr > > Course::getCourses() {
	return mCourses;
}

Course::Ptr Course::clone() {
	Poco::Tuple < int, int, int, int > time = getTime();
	return new Course(
			getSubject(),
			getTutor(),
			getPlace(),
			getType(),
			getDay(),
			getWeekCycle(),
			time.get<0>(),
			time.get<1>(),
			time.get<2>(),
			time.get<3>()
	);
}

Course::Ptr Course::fromJSON( Poco::JSON::Object::Ptr json ) {
	{
		std::vector < std::string > requiredKeys;
		requiredKeys.push_back( SUBJECT );
		requiredKeys.push_back( TUTOR );
		requiredKeys.push_back( PLACE );
		requiredKeys.push_back( COURSE_TYPE );
		requiredKeys.push_back( DAY_OF_WEEK );
		requiredKeys.push_back( WEEK_CYCLE );
		requiredKeys.push_back( TIME );

		typedef std::vector < std::string >::iterator It;
		std::vector < std::string > missingKeys;
		for( It it=requiredKeys.begin(), itEnd=requiredKeys.end(); it!=itEnd; it++ ) {
			if( !json->has( *it ) ) {
				missingKeys.push_back( *it );
			}
		}
		if( !missingKeys.empty() ) {
			throw Poco::Exception( Poco::format(
					"Course JSON object is missing '%s' keys!",
					Poco::cat( std::string( ", " ), missingKeys.begin(), missingKeys.end() )
			));
		}
	}

	Settings::CourseType courseType = (Settings::CourseType)0;
	Settings::DayOfWeek dayOfWeek 	= (Settings::DayOfWeek)0;
	Settings::WeekCycle weekCycle 	= (Settings::WeekCycle)0;
	{
		std::string courseTypeStr = json->get( COURSE_TYPE ).toString();
		std::map < std::string, Settings::CourseType > courseTypeIDs = Settings::getInstance()->getCourseTypeIDs();
		if( !courseTypeIDs.count( courseTypeStr ) ) {
			throw Poco::Exception( Poco::format( "Course JSON key '%s' cannot '%s' value!", COURSE_TYPE, courseTypeStr ) );
		}
		courseType = courseTypeIDs[courseTypeStr];
	}
	{
		std::string dayOfWeekStr = json->get( DAY_OF_WEEK ).toString();
		std::map < std::string, Settings::DayOfWeek > dayOfWeekIDs = Settings::getInstance()->getDaysIDs();
		if( !dayOfWeekIDs.count( dayOfWeekStr ) ) {
			throw Poco::Exception( Poco::format( "Course JSON key '%s' cannot '%s' value!", DAY_OF_WEEK, dayOfWeekStr ) );
		}
		dayOfWeek = dayOfWeekIDs[dayOfWeekStr];
	}
	{
		std::string weekCycleStr = json->get( WEEK_CYCLE ).toString();
		std::map < std::string, Settings::WeekCycle > weekCycleIDs = Settings::getInstance()->getWeekCycleIDs();
		if( !weekCycleIDs.count( weekCycleStr ) ) {
			throw Poco::Exception( Poco::format( "Course JSON key '%s' cannot '%s' value!", WEEK_CYCLE, weekCycleStr ) );
		}
		weekCycle = weekCycleIDs[weekCycleStr];
	}

	Poco::StringTokenizer st( json->get( TIME ).toString(), "-:" );
	{
		std::string errMsg = Poco::format( "Course JSON key '%s' must have value in format 'HH:MM-HH:MM'!", TIME );
		if( st.count() != 4 ) {
			throw Poco::Exception( errMsg );
		}
		typedef Poco::StringTokenizer::Iterator It;
		for( It it=st.begin(), itEnd=st.end(); it!=itEnd; it++ ) {
			if( it->size()>2 || it->size()==0 ) {
				throw Poco::Exception( errMsg );
			}
		}
	}

	int startHour 	= 0;
	int startMinute = 0;
	int endHour 	= 0;
	int endMinute 	= 0;
	try {
		startHour 	= Poco::NumberParser::parse( st[0] );
		startMinute = Poco::NumberParser::parse( st[1] );
		endHour 	= Poco::NumberParser::parse( st[2] );
		endMinute	= Poco::NumberParser::parse( st[3] );
	}
	catch( const Poco::Exception& e ) {
		throw Poco::Exception( Poco::format( "Course JSON key '%s' parsing error: %s", TIME, e.displayText() ) );
	}

	return new Course(
			json->get( SUBJECT ).toString(),
			json->get( TUTOR ).toString(),
			json->get( PLACE ).toString(),
			courseType,
			dayOfWeek,
			weekCycle,
			startHour,
			startMinute,
			endHour,
			endMinute
	);
}

Poco::JSON::Object::Ptr Course::toJSON() const {
	Poco::JSON::Object::Ptr json = new Poco::JSON::Object;

	json->set( SUBJECT, getSubject() );
	json->set( TUTOR, getTutor() );
	json->set( PLACE, getPlace() );
	json->set( COURSE_TYPE, mSettings->getCourseTypeLabels()[getType()] );
	json->set( DAY_OF_WEEK, mSettings->getDaysLabels()[getDay()] );
	json->set( WEEK_CYCLE, mSettings->getWeekCycleLabels()[getWeekCycle()] );
	json->set( TIME, getTimeStr() );

	return json;
}

std::string Course::getSubject() const {
	return mSubject;
}
std::string Course::getTutor() const {
	return mTutor;
}
std::string Course::getPlace() const {
	return mPlace;
}
Settings::CourseType Course::getType() const {
	return mType;
}
Settings::DayOfWeek Course::getDay() const {
	return mDay;
}
Settings::WeekCycle Course::getWeekCycle() const {
	return mWeekCycle;
}
Poco::Tuple < int, int, int, int > Course::getTime() const {
	return Poco::Tuple < int, int, int, int >(
			mStartHour,
			mStartMinute,
			mEndHour,
			mEndMinute
	);
}
std::string Course::getTimeStr() const {
	return Poco::format(
			"%d:%02d-%d:%02d",
			mStartHour,
			mStartMinute,
			mEndHour,
			mEndMinute
	);
}

void Course::setSubject( std::string subject ) {
	mSubject = subject;
}
void Course::setTutor( std::string tutor ) {
	mTutor = tutor;
}
void Course::setPlace( std::string place ) {
	mPlace = place;
}
void Course::setType( Settings::CourseType type ) {
	mType = type;
}
void Course::setDay( Settings::DayOfWeek day ) {
	mDay = day;
}
void Course::setWeekCycle( Settings::WeekCycle weekCycle ) {
	mWeekCycle = weekCycle;
}
void Course::setTime( int startHour, int startMinute, int endHour, int endMinute ) {
	verifyTime( startHour, startMinute, endHour, endMinute );
	mStartHour = startHour;
	mStartMinute = startMinute;
	mEndHour = endHour;
	mEndMinute = endMinute;
}

bool Course::isVisible() const {
	if( !mSettings->isDayVisible( mDay ) ) {
		return false;
	}
	std::pair < int, int > hourRange = mSettings->getRangeHours();
	if( ((mEndHour*60)+mEndMinute) < (hourRange.first*60) ) {
		return false;
	}
	if( ((mStartHour*60)+mStartMinute) > (hourRange.second*60) ) {
		return false;
	}

	return true;
}

void Course::verifyTime( int startHour, int startMinute, int endHour, int endMinute ) const {
	if( startMinute < 0 ) {
		throw Poco::Exception( "Start minutes value negative!" );
	}
	if( startMinute > 59 ) {
		throw Poco::Exception( "Start minutes value greater than 59!" );
	}
	if( endMinute < 0 ) {
		throw Poco::Exception( "End minutes value negative!" );
	}
	if( endMinute > 59 ) {
		throw Poco::Exception( "End minutes value greater than 59!" );
	}

	std::pair < int, int > range = mSettings->getRangeHours();
	range = std::pair < int, int >( range.first*60, range.second*60 );

	int start = ( startHour * 60 ) + startMinute;
	int end = ( endHour * 60 ) + endMinute;

	if( end < start ) {
		throw Poco::Exception( "Course start time cannot exceed end time!" );
	}
	if( ( end - start ) < mSettings->getMinDuration() ) {
		throw Poco::Exception( Poco::format( "Course duration cannot be shorter than %d minutes!", mSettings->getMinDuration() ) );
	}
}

}
}
