/* Copyright by: P.J. Grochowski */

#include "Frame.h"

#include "ScheduleParser.h"

#include "gdev/guiWX/LangSwitcher.h"
#include "gdev/utility/Strings.h"
#include "gdev/utility/Resources.h"
#include "gdev/utility/JSON.h"
#include "gdev/iCal/ICalParser.h"
#include "gdev/appWx/App.h"

#include <wx/sizer.h>
#include <wx/menu.h>
#include <wx/statline.h>
#include <wx/dcclient.h>
#include <wx/dcgraph.h>
#include <wx/dcmemory.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>

#include <Poco/FileStream.h>
#include <Poco/StreamCopier.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>
#include <Poco/Tuple.h>

#include <sstream>
#include <math.h>
#include <iomanip>

namespace gdev {
namespace spSchedule {

// Save file JSON keys:
static const std::string COURSES 	= "courses";

static const std::string EXT_SPSCH = ".spsch";
std::string fixPath( const std::string& path, const std::string& ext )
{
	if( (path.size() < ext.size() ) || (path.substr( path.size()-ext.size() ).compare( ext )) ) {
		return path + ext;
	}
	return path;
}

/*/----------------------------------------------------------------------/*/

Frame::Frame():
		wxFrame( NULL, wxID_ANY, gdev::appWx::App::getName() + " " + gdev::appWx::App::getVersion(), wxDefaultPosition, wxDefaultSize ),
		gdev::utility::Loggable( "Frame" )
{
	// Set icon:
	{
		wxIcon icon;
		icon.CopyFromBitmap( _getBitmapWX( "appicon.png" ) );
		SetIcon( icon );
	}

	// Set dialog resizing hints:
	SetSizeHints( wxSize( 800, 600 ) );

	// Set background window color:
	SetForegroundColour( wxColour( 0, 0, 0 ) );
	SetBackgroundColour( wxColour( 255, 255, 255 ) );

	// Initialize members:
	mSettings = Settings::getInstance();
	mCourses = Course::getCourses();

	// Create dialogs:
	mDlgAbout 		= new DialogAbout( this );
	mDlgForm 		= new DialogForm( this );
	mDlgSettings 	= new DialogSettings( this );
	mDlgResolution 	= new DialogResolution( this );

	// Initialize controls:
	initCtrl();

	// Initialize menu:
	initMenu();

	// Center window:
	Centre( wxBOTH );

	// Bind events:
	Bind( wxEVT_CLOSE_WINDOW, &Frame::onClose, this );
}

Frame::~Frame() {
	Unbind( wxEVT_CLOSE_WINDOW, &Frame::onClose, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onAdd, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onSave, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onLoad, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onImport, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onExport, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onClear, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onSettings, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onExit, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onAbout, this );
	Unbind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onLanguage, this );
}

void Frame::onClose( wxCloseEvent& ) {
	if( !mCourses->empty() && wxMessageBox(
			_getStrWX( "msgbox_warning_exit_unclean" ),
			_getStrWX( "warning" ),
			wxYES_NO|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
		) != wxYES )
	{
		return;
	}
	Destroy();
}
void Frame::onNotify( wxThreadEvent& ) {
	mPanelSchedule->refresh();
}

void Frame::onAdd( wxCommandEvent& ) {
	Course::Ptr course = NULL;
	if( mDlgForm->show( course ) ) {
		mCourses->push_back( course );
		mPanelSchedule->refresh();
	}
}
void Frame::onSave( wxCommandEvent& ) {
	if( mCourses->empty() )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_question_save_empty" ),
				_getStrWX( "question" ),
				wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}

	std::string sPath;
	{
		wxFileDialog fDlg(
				this,
				_getStrWX( "dialog_file_save" ),
				wxEmptyString,
				wxEmptyString,
				_getStrWX( "dialog_file_wildcard_schedule" ),
				wxFD_SAVE|wxFD_OVERWRITE_PROMPT
		);
		if( fDlg.ShowModal() == wxID_CANCEL ) {
			return;
		}
		sPath = fixPath( _fixStrSTL( fDlg.GetPath() ), EXT_SPSCH );
	}

	log().debug( "Save schedule to '%s'.", sPath );

	new gdev::guiWx::DialogProgress < Frame >( *this, &Frame::processTask, Poco::Dynamic::Var( sPath ), (std::size_t)ID_SAVE );
}
void Frame::onLoad( wxCommandEvent& ) {
	if( !mCourses->empty() )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_warning_board_rewrite" ),
				_getStrWX( "warning" ),
				wxYES_NO|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}

	std::string sPath;
	{
		wxFileDialog fDlg(
				this,
				_getStrWX( "dialog_file_open" ),
				wxEmptyString,
				wxEmptyString,
				_getStrWX( "dialog_file_wildcard_schedule" ),
				wxFD_OPEN|wxFD_FILE_MUST_EXIST
		);

		if( fDlg.ShowModal() == wxID_CANCEL ) {
			return;
		}

		sPath = fixPath( _fixStrSTL( fDlg.GetPath() ), EXT_SPSCH );
	}

	log().debug( "Load save file from path '%s'.", sPath );

	new gdev::guiWx::DialogProgress < Frame >( *this, &Frame::processTask, Poco::Dynamic::Var( sPath ), (std::size_t)ID_LOAD );
}
void Frame::onImport( wxCommandEvent& ) {
	if( !mCourses->empty() )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_warning_board_rewrite" ),
				_getStrWX( "warning" ),
				wxYES_NO|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}

	std::string sPath;
	{
		wxFileDialog fDlg(
				this,
				_getStrWX( "dialog_file_import" ),
				wxEmptyString,
				wxEmptyString,
				_getStrWX( "dialog_file_wildcard_ical" ),
				wxFD_OPEN|wxFD_FILE_MUST_EXIST
		);

		if( fDlg.ShowModal() == wxID_CANCEL ) {
			return;
		}

		sPath = _fixStrSTL( fDlg.GetPath() );
	}

	log().debug( "Import file from path '%s'.", sPath );

	new gdev::guiWx::DialogProgress < Frame >( *this, &Frame::processTask, Poco::Dynamic::Var( sPath ), (std::size_t)ID_IMPORT );
}
void Frame::onExport( wxCommandEvent& ) {
	if( mCourses->empty() )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_question_export_empty" ),
				_getStrWX( "question" ),
				wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}

	std::string sPath;
	ImageFormat imgFormat = PNG;
	{
		wxFileDialog fDlg(
				this,
				_getStrWX( "dialog_file_export" ),
				wxEmptyString,
				wxEmptyString,
				_getStrWX( "dialog_file_wildcard_picture" ),
				wxFD_SAVE|wxFD_OVERWRITE_PROMPT
		);
		if( fDlg.ShowModal() == wxID_CANCEL ) {
			return;
		}
		sPath = _fixStrSTL( fDlg.GetPath() );
		imgFormat = (ImageFormat)fDlg.GetFilterIndex();

		switch( (int)imgFormat )
		{
			case (int)PNG: {
				sPath = fixPath( sPath, ".png" );
			} break;
			case (int)JPG: {
				sPath = fixPath( sPath, ".jpg" );
			} break;
			case (int)BMP: {
				sPath = fixPath( sPath, ".bmp" );
			} break;
			default: {
				log().error( "Unsupported image format requested by id '%?d'!", (int)imgFormat );
				wxMessageBox(
						_getStrWX( "msgbox_error_image_format" ),
						_getStrWX( "error" ),
						wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
				);
			} return;
		}
	}
	wxSize size( 0, 0 );
	if( !mDlgResolution->show( size ) ) {
		return;
	}

	log().debug( "Export image to '%s'.", sPath );

	new gdev::guiWx::DialogProgress < Frame >( *this, &Frame::processTask, Poco::Dynamic::Var( Poco::Tuple < std::string, wxSize, ImageFormat >( sPath, size, imgFormat ) ), (std::size_t)ID_EXPORT );
}
void Frame::onClear( wxCommandEvent& ) {
	if( wxMessageBox(
			_getStrWX( "msgbox_question_clear_board" ),
			_getStrWX( "question" ),
			wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
		) == wxYES )
	{
		mCourses->clear();
		mPanelSchedule->refresh();
	}
}
void Frame::onSettings( wxCommandEvent& ) {
	if( mDlgSettings->show() ) {
		mPanelSchedule->refresh();
		{
			typedef std::vector < Course::Ptr >::iterator It;
			for( It it=mCourses->begin(), itEnd=mCourses->end(); it!=itEnd; it++ ) {
				if( !(*it)->isVisible() ) {
					wxMessageBox(
							_getStrWX( "msgbox_warning_course_invisible" ),
							_getStrWX( "warning" ),
							wxOK|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
					);
					break;
				}
			}
		}
	}
}
void Frame::onExit( wxCommandEvent& ) {
	Close( true );
}

void Frame::onAbout( wxCommandEvent& ) {
	mDlgAbout->show();
}

void Frame::onLanguage( wxCommandEvent& event ) {
	int iID = event.GetId();
	std::vector < std::string > langs = gdev::utility::Strings::getInstance()->getLangs();

	gdev::guiWx::LangSwitcher::getInstance()->setLang( langs.at( iID - IDS_COUNT ) );
	log().information( "Language changed to: '%s'", langs.at( iID - IDS_COUNT ) );
}

void Frame::initMenu()
{
	// Create menus:
	wxMenuBar* menuBar = new wxMenuBar( 0 );
	wxMenu* menuFile = new wxMenu();
	wxMenu* menuHelp = new wxMenu();
	wxMenu* menuLang = new wxMenu();

	// Create menu vectors for language switcher:
	std::vector < std::pair < std::size_t, std::string > > menus;
	std::vector < std::pair < std::size_t, std::string > > items;

	// Append items to 'File':
	const std::string MENU_FILE_ADD 		= "menu_file_add";
	const std::string MENU_FILE_SAVE 		= "menu_file_save";
	const std::string MENU_FILE_LOAD 		= "menu_file_load";
	const std::string MENU_FILE_IMPORT 		= "menu_file_import";
	const std::string MENU_FILE_EXPORT 		= "menu_file_export";
	const std::string MENU_FILE_CLEAR 		= "menu_file_clear";
	const std::string MENU_FILE_SETTINGS 	= "menu_file_settings";
	const std::string MENU_FILE_EXIT 		= "menu_file_exit";
	menuFile->Append( ID_ADD, _getStrWX( MENU_FILE_ADD ) );
	menuFile->Append( ID_SAVE, _getStrWX( MENU_FILE_SAVE ) );
	menuFile->Append( ID_LOAD, _getStrWX( MENU_FILE_LOAD ) );
	menuFile->Append( ID_IMPORT, _getStrWX( MENU_FILE_IMPORT ) );
	menuFile->Append( ID_EXPORT, _getStrWX( MENU_FILE_EXPORT ) );
	menuFile->Append( ID_CLEAR, _getStrWX( MENU_FILE_CLEAR ) );
	menuFile->Append( ID_SETTINGS, _getStrWX( MENU_FILE_SETTINGS ) );
	menuFile->Append( ID_EXIT, _getStrWX( MENU_FILE_EXIT ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_ADD, MENU_FILE_ADD ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_SAVE, MENU_FILE_SAVE ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_LOAD, MENU_FILE_LOAD ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_IMPORT, MENU_FILE_IMPORT ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_EXPORT, MENU_FILE_EXPORT ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_CLEAR, MENU_FILE_CLEAR ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_SETTINGS, MENU_FILE_SETTINGS ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_EXIT, MENU_FILE_EXIT ) );

	// Append items to 'Help':
	const std::string MENU_HELP_ABOUT = "menu_help_about";
	menuHelp->Append( ID_ABOUT, _getStrWX( MENU_HELP_ABOUT ) );
	items.push_back( std::pair < std::size_t, std::string >( (std::size_t)ID_ABOUT, MENU_HELP_ABOUT ) );

	// Append items to 'Language':
	std::vector < std::string > langs = gdev::utility::Strings::getInstance()->getLangs();
	for( std::size_t i=0; i<langs.size(); i++ ) {
		menuLang->AppendRadioItem( i + IDS_COUNT, _fixStrWX( langs.at( i ) ) );
	}
	menuLang->Check( menuLang->FindItem( _fixStrWX( gdev::utility::Strings::getInstance()->getLang() ) ), true );

	// Append menus to bar:
	const std::string MENU_FILE = "menu_file";
	const std::string MENU_HELP = "menu_help";
	const std::string MENU_LANG = "menu_language";
	menuBar->Append( menuFile, _getStrWX( MENU_FILE ) );
	menuBar->Append( menuHelp, _getStrWX( MENU_HELP ) );
	menuBar->Append( menuLang, _getStrWX( MENU_LANG ) );
	menus.push_back( std::pair < std::size_t, std::string >( 0, MENU_FILE ) );
	menus.push_back( std::pair < std::size_t, std::string >( 1, MENU_HELP ) );
	menus.push_back( std::pair < std::size_t, std::string >( 2, MENU_LANG ) );

	// Set bar:
	SetMenuBar( menuBar );
	gdev::guiWx::LangSwitcher::getInstance()->addMenuBar( menuBar, menus, items );

	// Bind events:
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onAdd, this, ID_ADD );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onSave, this, ID_SAVE );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onLoad, this, ID_LOAD );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onImport, this, ID_IMPORT );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onExport, this, ID_EXPORT );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onClear, this, ID_CLEAR );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onSettings, this, ID_SETTINGS );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onExit, this, ID_EXIT );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onAbout, this, ID_ABOUT );
	Bind( wxEVT_COMMAND_MENU_SELECTED, &Frame::onLanguage, this, IDS_COUNT, IDS_COUNT + langs.size() );
}

void Frame::initCtrl()
{
	wxBoxSizer* sizer = new wxBoxSizer( wxVERTICAL );

	mPanelSchedule = new PanelSchedule( this, mDlgForm );
	sizer->Add( mPanelSchedule, 1, wxEXPAND, 5 );

	wxStaticLine* line = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	sizer->Add( line, 0, wxEXPAND, 5 );

	/*wxBoxSizer* sizerStatus = new wxBoxSizer( wxVERTICAL );

	mCtrlStatus = new wxStaticText( this, wxID_ANY, wxT("<status>"), wxDefaultPosition, wxDefaultSize, 0 );
	mCtrlStatus->Wrap( -1 );
	sizerStatus->Add( mCtrlStatus, 0, wxRIGHT|wxLEFT|wxALIGN_RIGHT, 5 );

	sizer->Add( sizerStatus, 0, wxEXPAND, 5 );*/

	SetSizer( sizer );
	Layout();
	Fit();
}

bool Frame::processTask( gdev::guiWx::DialogProgress < Frame >& dlgProg, Poco::Dynamic::Var dVar, std::size_t caseIdx )
{
	switch( caseIdx )
	{
		case ID_SAVE: {
			Poco::JSON::Object::Ptr json = new Poco::JSON::Object;
			{
				Poco::JSON::Array::Ptr jsonItem = new Poco::JSON::Array;
				typedef std::vector < Course::Ptr >::iterator It;
				for( It it=mCourses->begin(), itEnd=mCourses->end(); it!=itEnd; it++ ) {
					jsonItem->add( (*it)->toJSON() );
				}
				json->set( COURSES, jsonItem );
			}

			std::string jsonStr = gdev::utility::JSON::toString(json, !gdev::appWx::App::isDebug());
			Poco::FileOutputStream ostr( dVar.toString() );
			std::stringstream sstr( jsonStr );
			while(Poco::StreamCopier::copyStream(sstr, ostr));
			ostr.flush();
			ostr.close();
		} return false;

		case ID_LOAD: {
			try {
				Poco::JSON::Object::Ptr json;
				{
					std::string content;
					Poco::FileInputStream istr( dVar.toString() );
					while(Poco::StreamCopier::copyToString(istr, content));
					json = gdev::utility::JSON::toJSON( content )->extract < Poco::JSON::Object::Ptr >();
				}

				{
					std::vector < std::string > requiredKeys;
					requiredKeys.push_back( COURSES );

					typedef std::vector < std::string >::iterator It;
					std::vector < std::string > missingKeys;
					for( It it=requiredKeys.begin(), itEnd=requiredKeys.end(); it!=itEnd; it++ ) {
						if( !json->has( *it ) ) {
							missingKeys.push_back( *it );
						}
					}
					if( !missingKeys.empty() ) {
						throw Poco::Exception( Poco::format(
								"Save JSON file is missing '%s' keys!",
								Poco::cat( std::string( ", " ), missingKeys.begin(), missingKeys.end() )
						));
					}
				}

				std::vector < Course::Ptr > courses;
				{
					Poco::JSON::Array::Ptr jsonItem = json->getArray( COURSES );
					for( std::size_t i=0; i<jsonItem->size(); i++ ) {
						courses.push_back( Course::fromJSON( jsonItem->getObject( i ) ) );
					}
				}
				mCourses->swap( courses );
				mPanelSchedule->refresh();

				return true;

			} // Handle exceptions:
			catch( const Poco::Exception& e ) {
				log().error( e.displayText() );
			}
			catch( const std::exception& e ) {
				log().error( std::string( e.what() ) );
			}
			catch( ... ) {
				log().error( "Application encountered unknown error!" );
			}
			std::string msg = Poco::format(
					gdev::utility::Strings::getInstance()->getString( "msgbox_error_save_corrupted" ),
					dVar.toString()
			);
			dlgProg.sendError( _fixStrWX( msg ) );
		} return false;

		case ID_IMPORT: {
			try {
				std::string content;
				Poco::FileInputStream istr( dVar.toString() );
				while( Poco::StreamCopier::copyToString( istr, content ) );
				std::vector < Course::Ptr > courses = ScheduleParser::parseICalEvents( gdev::iCal::ICalParser( content ).getEvents() );
				mCourses->swap( courses );
				mPanelSchedule->refresh();

				{
					typedef std::vector < Course::Ptr >::iterator It;
					for( It it=mCourses->begin(), itEnd=mCourses->end(); it!=itEnd; it++ ) {
						if( !(*it)->isVisible() ) {
							dlgProg.sendWarn( _getStrWX( "msgbox_warning_course_invisible" ) );
							break;
						}
					}
				}

				return true;

			} // Handle exceptions:
			catch( const Poco::Exception& e ) {
				log().error( e.displayText() );
			}
			catch( const std::exception& e ) {
				log().error( std::string( e.what() ) );
			}
			catch( ... ) {
				log().error( "Application encountered unknown error!" );
			}
			dlgProg.sendError( _getStrWX( "msgbox_error_parsing_ical" ) );
		} return false;

		case ID_EXPORT: {
			Poco::Tuple < std::string, wxSize, ImageFormat > data = dVar.extract < Poco::Tuple < std::string, wxSize, ImageFormat > >();
			std::string sPath = data.get<0>();
			wxSize size = data.get<1>();
			ImageFormat imgFormat = data.get<2>();

			wxBitmap bmp( size );
			wxMemoryDC mdc( bmp );
			wxGCDC dc( mdc );

			wxSize bmpSize = wxSize(bmp.GetWidth(), bmp.GetHeight());
			SchedulePainter(bmpSize).drawBoard(dc);

			mdc.SelectObject( wxNullBitmap );

			wxImage img = bmp.ConvertToImage();
			//img.Blur( ceil((img.GetHeight()*0.125)/100) ); //TODO: Decide if blur needed!

			switch( (int)imgFormat )
			{
				case (int)PNG: {
					img.SaveFile( _fixStrWX( sPath ), wxBITMAP_TYPE_PNG );
				} break;
				case (int)JPG: {
					img.SaveFile( _fixStrWX( sPath ), wxBITMAP_TYPE_JPEG );
				} break;
				case (int)BMP: {
					img.SaveFile( _fixStrWX( sPath ), wxBITMAP_TYPE_BMP );
				} break;
				default: {
					dlgProg.sendError( _getStrWX( "msgbox_error_image_format" ) );
					log().error( "Unsupported image format requested by id '%?d'!", (int)imgFormat );
				} break;
			}
		} return false;
	}

	return false;
}

}
}
