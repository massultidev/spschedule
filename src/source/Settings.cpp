/* Copyright by: P.J. Grochowski */

#include "Settings.h"

#include <Poco/File.h>
#include <Poco/Path.h>
#include <Poco/FileStream.h>
#include <Poco/StreamCopier.h>

#include "gdev/utility/Strings.h"
#include "gdev/utility/JSON.h"
#include "gdev/appWx/App.h"

namespace gdev {
namespace spSchedule {

// Save file JSON keys:
static const std::string EDGES_RADIUS 		= "edges_radius";
static const std::string HOUR_BOUND_LOWER 	= "hour_bound_lower";
static const std::string HOUR_BOUND_UPPER 	= "hour_bound_upper";
static const std::string COURSE_TYPE_COLOR 	= "course_type_color";
static const std::string DAY_VISIBILITY 	= "day_visibility";

Settings::Ptr Settings::mInstance = NULL;

Settings::Settings() {
	clear();
}
Settings::~Settings() {
}

Settings::Ptr Settings::getInstance() {
	if(mInstance.isNull()) {
		mInstance = new Settings();
	}
	return mInstance;
}

void Settings::load() {
	if(!doSettingsFileExist()) {
		clear();
		return;
	}
	readFromFile();
}

void Settings::save() {
	saveToFile();
}

void Settings::reset() {
	clear();
	if(doSettingsFileExist()) {
		Poco::File(getFilePathSettings()).remove();
	}
}

void Settings::clear() {
	mMinDuration = 45;
	mEdgesRadius = 10;
	mBoundsEdgesRadius = std::pair < int, int >( 0, 10 );
	mRangeHours = std::pair < int, int >( 7, 21 );
	mCourseTypes[LECTURE] 		= std::pair < std::string, wxColour >( "course_type_lecture", 		wxColour( 30, 160, 40 ) ); // 	[GREEN]
	mCourseTypes[LABORATORY] 	= std::pair < std::string, wxColour >( "course_type_laboratory", 	wxColour( 70, 130, 250 ) ); // 	[BLUE]
	mCourseTypes[EXERCISES] 	= std::pair < std::string, wxColour >( "course_type_exercises", 	wxColour( 250, 40, 40 ) ); // 	[RED]
	mCourseTypes[PROJECT] 		= std::pair < std::string, wxColour >( "course_type_project", 		wxColour( 40, 250, 250 ) ); // 	[LIGHTBLUE]
	mCourseTypes[SEMINAR] 		= std::pair < std::string, wxColour >( "course_type_seminar", 		wxColour( 250, 250, 40 ) ); // 	[YELLOW]
	mCourseTypes[OTHER] 		= std::pair < std::string, wxColour >( "course_type_other", 		wxColour( 250, 180, 40 ) ); // 	[ORANGE]
	mDays[MONDAY] 		= std::pair < std::string, bool >( "day_monday", 	true );
	mDays[TUESDAY] 		= std::pair < std::string, bool >( "day_tuesday", 	true );
	mDays[WEDNESDAY] 	= std::pair < std::string, bool >( "day_wednesday", true );
	mDays[THURSDAY] 	= std::pair < std::string, bool >( "day_thursday", 	true );
	mDays[FRIDAY] 		= std::pair < std::string, bool >( "day_friday", 	true );
	mDays[SATURDAY] 	= std::pair < std::string, bool >( "day_saturday", 	false );
	mDays[SUNDAY] 		= std::pair < std::string, bool >( "day_sunday", 	false );
	mWeekCycles[BOTH] 	= "weekcycle_both";
	mWeekCycles[ODD] 	= "weekcycle_odd";
	mWeekCycles[EVEN] 	= "weekcycle_even";
}

std::string Settings::getFilePathSettings() const {
	return Poco::Path(gdev::appWx::App::getDirSettings(), "settings.json").toString();
}

bool Settings::doSettingsFileExist() const {
	const std::string filePathSettings = getFilePathSettings();
	Poco::File fileSettings(filePathSettings);
	return (fileSettings.exists() && !fileSettings.isDirectory());
}

void Settings::readFromFile() {
	std::string content;
	Poco::FileInputStream istr(getFilePathSettings());
	while(Poco::StreamCopier::copyToString(istr, content));
	Poco::JSON::Object::Ptr json = gdev::utility::JSON::toJSON(content)->extract<Poco::JSON::Object::Ptr>();
	setFromJSON(json);
}

void Settings::saveToFile() {
	Poco::JSON::Object::Ptr json = toJSON();
	std::string jsonStr = gdev::utility::JSON::toString(json, !gdev::appWx::App::isDebug());
	Poco::FileOutputStream ostr(getFilePathSettings());
	std::stringstream sstr(jsonStr);
	while(Poco::StreamCopier::copyStream(sstr, ostr));
	ostr.flush();
	ostr.close();
}

int Settings::getMinDuration() const {
	return mMinDuration;
}
int Settings::getEdgesRadius() const {
	return mEdgesRadius;
}
std::pair < int, int > Settings::getBoundsEdgesRadius() const {
	return mBoundsEdgesRadius;
}
std::pair < int, int > Settings::getRangeHours() const {
	return mRangeHours;
}
std::map < Settings::CourseType, wxColour > Settings::getCourseTypesColours() const
{
	std::map < Settings::CourseType, wxColour > typeColors;
	typedef std::map < CourseType, std::pair < std::string, wxColour > >::iterator It;
	for( It it=mCourseTypes.begin(), itEnd=mCourseTypes.end(); it!=itEnd; it++ ) {
		typeColors[it->first] = it->second.second;
	}
	return typeColors;
}
wxColour Settings::getCourseTypeColour( CourseType courseTypeID ) const {
	return mCourseTypes[courseTypeID].second;
}
std::map < Settings::DayOfWeek, bool > Settings::getDaysVisibility() const
{
	std::map < Settings::DayOfWeek, bool > dayVisibility;
	typedef std::map < DayOfWeek, std::pair < std::string, bool > >::const_iterator It;
	for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ ) {
		dayVisibility[it->first] = it->second.second;
	}
	return dayVisibility;
}
bool Settings::isDayVisible( DayOfWeek day ) const {
	return mDays[day].second;
}
std::size_t Settings::visibleDaysCount() const
{
	std::size_t vdc = 0;
	typedef std::map < DayOfWeek, std::pair < std::string, bool > >::const_iterator It;
	for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ )
	{
		if( it->second.second ) {
			vdc++;
		}
	}
	return vdc;
}

std::map < Settings::CourseType, std::string > Settings::getCourseTypeLabels() const
{
	std::map < Settings::CourseType, std::string > typeLabels;
	typedef std::map < CourseType, std::pair < std::string, wxColour > >::const_iterator It;
	for( It it=mCourseTypes.begin(), itEnd=mCourseTypes.end(); it!=itEnd; it++ ) {
		typeLabels[it->first] = it->second.first;
	}
	return typeLabels;
}
std::map < std::string, Settings::CourseType > Settings::getCourseTypeIDs() const
{
	std::map < std::string, CourseType > typeIDs;
	typedef std::map < CourseType, std::pair < std::string, wxColour > >::const_iterator It;
	for( It it=mCourseTypes.begin(), itEnd=mCourseTypes.end(); it!=itEnd; it++ ) {
		typeIDs[it->second.first] = it->first;
	}
	return typeIDs;
}
std::vector < std::string > Settings::getCourseTypeLabelsVector() const
{
	std::vector < std::string > typeLabels;
	typedef std::map < CourseType, std::pair < std::string, wxColour > >::const_iterator It;
	for( It it=mCourseTypes.begin(), itEnd=mCourseTypes.end(); it!=itEnd; it++ ) {
		typeLabels.push_back( it->second.first );
	}
	return typeLabels;
}
std::map < Settings::DayOfWeek, std::string > Settings::getDaysLabels() const
{
	std::map < Settings::DayOfWeek, std::string > dayLabels;
	typedef std::map < DayOfWeek, std::pair < std::string, bool > >::const_iterator It;
	for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ ) {
		dayLabels[it->first] = it->second.first;
	}
	return dayLabels;
}
std::map < std::string, Settings::DayOfWeek > Settings::getDaysIDs() const
{
	std::map < std::string, Settings::DayOfWeek > dayIDs;
	typedef std::map < DayOfWeek, std::pair < std::string, bool > >::const_iterator It;
	for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ ) {
		dayIDs[it->second.first] = it->first;
	}
	return dayIDs;
}
std::vector < std::string > Settings::getDaysLabelsVector() const
{
	std::vector < std::string > dayLabels;
	typedef std::map < DayOfWeek, std::pair < std::string, bool > >::const_iterator It;
	for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ ) {
		dayLabels.push_back( it->second.first );
	}
	return dayLabels;
}
std::map < Settings::WeekCycle, std::string > Settings::getWeekCycleLabels() const {
	return mWeekCycles;
}
std::map < std::string, Settings::WeekCycle > Settings::getWeekCycleIDs() const
{
	std::map < std::string, Settings::WeekCycle > weekCycleIDs;
	typedef std::map < Settings::WeekCycle, std::string >::iterator It;
	for( It it=mWeekCycles.begin(), itEnd=mWeekCycles.end(); it!=itEnd; it++ ) {
		weekCycleIDs[it->second] = it->first;
	}
	return weekCycleIDs;
}
std::vector < std::string > Settings::getWeekCycleLabelsVector() const
{
	std::vector < std::string > weekCycleLabels;
	typedef std::map < Settings::WeekCycle, std::string >::const_iterator It;
	for( It it=mWeekCycles.begin(), itEnd=mWeekCycles.end(); it!=itEnd; it++ ) {
		weekCycleLabels.push_back( it->second );
	}
	return weekCycleLabels;
}

void Settings::setEdgesRadius( int radius )
{
	if( radius < mBoundsEdgesRadius.first ) {
		throw Poco::Exception( "Edges radius beneath range!" );
	}
	if( radius > mBoundsEdgesRadius.second ) {
		throw Poco::Exception( "Edges radius above range!" );
	}
	mEdgesRadius = radius;
}
void Settings::setRangeHour( std::pair < int, int > range )
{
	if( range.first < 0 ) {
		throw Poco::Exception( "Lower hour range has negative value!" );
	}
	if( range.second > 24 ) {
		throw Poco::Exception( "Upper hour range is greater than 24!" );
	}
	if( range.second < range.first ) {
		throw Poco::Exception( "Upper hour range is lower than lower range!" );
	}
	if( range.second == range.first ) {
		throw Poco::Exception( "There must be at least one hour in range!" );
	}
	mRangeHours = range;
}
void Settings::setCourseTypeColour( CourseType courseTypeID, wxColour color ) {
	mCourseTypes[courseTypeID] = std::pair < std::string, wxColour >( mCourseTypes[courseTypeID].first, color );
}
void Settings::setDayVisibility( DayOfWeek day, bool state ) {
	mDays[day].second = state;
}

void Settings::setFromJSON( Poco::JSON::Object::Ptr json )
{
	{
		std::vector < std::string > requiredKeys;
		requiredKeys.push_back( EDGES_RADIUS );
		requiredKeys.push_back( HOUR_BOUND_LOWER );
		requiredKeys.push_back( HOUR_BOUND_UPPER );
		requiredKeys.push_back( COURSE_TYPE_COLOR );
		requiredKeys.push_back( DAY_VISIBILITY );

		typedef std::vector < std::string >::iterator It;
		std::vector < std::string > missingKeys;
		for( It it=requiredKeys.begin(), itEnd=requiredKeys.end(); it!=itEnd; it++ ) {
			if( !json->has( *it ) ) {
				missingKeys.push_back( *it );
			}
		}
		if( !missingKeys.empty() ) {
			throw Poco::Exception( Poco::format(
					"Settings JSON object is missing '%s' keys!",
					Poco::cat( std::string( ", " ), missingKeys.begin(), missingKeys.end() )
			));
		}
	}

	int edgeRadius = 0;
	int hourBoundLower = 0;
	int hourBoundUpper = 0;
	try {
		edgeRadius = json->get( EDGES_RADIUS ).convert < int >();
		hourBoundLower = json->get( HOUR_BOUND_LOWER ).convert < int >();
		hourBoundUpper = json->get( HOUR_BOUND_UPPER ).convert < int >();
	}
	catch( const Poco::Exception& e ) {
		throw Poco::Exception( Poco::format( "Settings JSON number values parsing error: '%s", e.displayText() ) );
	}

	std::map < std::string, wxColour > courseTypesColors;
	{
		Poco::JSON::Object::Ptr jsonItem = json->getObject( COURSE_TYPE_COLOR );
		std::vector < std::string > lbl;
		{
			std::vector < std::string > missingKeys;
			typedef std::map < CourseType, std::pair < std::string, wxColour > >::iterator It;
			for( It it=mCourseTypes.begin(), itEnd=mCourseTypes.end(); it!=itEnd; it++ ) {
				if( !jsonItem->has( it->second.first ) ) {
					missingKeys.push_back( it->second.first );
				}
				lbl.push_back( it->second.first );
			}
			if( !missingKeys.empty() ) {
				throw Poco::Exception( Poco::format(
						"Settings JSON in sub-object '%s' is missing '%s' keys!",
						COURSE_TYPE_COLOR, Poco::cat( std::string( ", " ), missingKeys.begin(), missingKeys.end() )
				));
			}
		}
		{
			typedef std::vector < std::string >::iterator It;
			for( It it=lbl.begin(), itEnd=lbl.end(); it!=itEnd; it++ ) {
				std::string colorStr = jsonItem->get( *it ).toString();
				wxColour color( 0, 0, 0 );
				if( !color.Set( _fixStrWX( colorStr ) ) ) {
					throw Poco::Exception( Poco::format( "String '%s' is no valid HTML style color notation!", colorStr ) );
				}
				courseTypesColors[*it] = color;
			}
		}
	}

	std::map < std::string, bool > dayVisibility;
	{
		Poco::JSON::Object::Ptr jsonItem = json->getObject( DAY_VISIBILITY );
		std::vector < std::string > lbl;
		{
			std::vector < std::string > missingKeys;
			typedef std::map < DayOfWeek, std::pair < std::string, bool > >::iterator It;
			for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ ) {
				if( !jsonItem->has( it->second.first ) ) {
					missingKeys.push_back( it->second.first );
				}
				lbl.push_back( it->second.first );
			}
			if( !missingKeys.empty() ) {
				throw Poco::Exception( Poco::format(
						"Settings JSON in sub-object '%s' is missing '%s' keys!",
						DAY_VISIBILITY, Poco::cat( std::string( ", " ), missingKeys.begin(), missingKeys.end() )
				));
			}
		}
		try {
			typedef std::vector < std::string >::iterator It;
			for( It it=lbl.begin(), itEnd=lbl.end(); it!=itEnd; it++ ) {
				dayVisibility[*it] = jsonItem->get( *it ).extract < bool >();
			}
		}
		catch( const Poco::Exception& e ) {
			throw Poco::Exception( Poco::format( "Settings JSON in sub-object '%s' boolean values parsing error: '%s", DAY_VISIBILITY, e.displayText() ) );
		}
	}

	setEdgesRadius( edgeRadius );
	setRangeHour( std::pair < int, int >( hourBoundLower, hourBoundUpper ) );
	{
		std::map < std::string, CourseType > courseTypeIDs = getCourseTypeIDs();
		typedef std::map < std::string, wxColour >::iterator It;
		for( It it=courseTypesColors.begin(), itEnd=courseTypesColors.end(); it!=itEnd; it++ ) {
			setCourseTypeColour( courseTypeIDs[it->first], it->second );
		}
	}
	{
		std::map < std::string, DayOfWeek > dayIDs = getDaysIDs();
		typedef std::map < std::string, bool >::iterator It;
		for( It it=dayVisibility.begin(), itEnd=dayVisibility.end(); it!=itEnd; it++ ) {
			setDayVisibility( dayIDs[it->first], it->second );
		}
	}
}

Poco::JSON::Object::Ptr Settings::toJSON() const
{
	Poco::JSON::Object::Ptr json = new Poco::JSON::Object;

	json->set( EDGES_RADIUS, mEdgesRadius );
	json->set( HOUR_BOUND_LOWER, mRangeHours.first );
	json->set( HOUR_BOUND_UPPER, mRangeHours.second );

	{
		Poco::JSON::Object::Ptr jsonItem = new Poco::JSON::Object;
		typedef std::map < CourseType, std::pair < std::string, wxColour > >::iterator It;
		for( It it=mCourseTypes.begin(), itEnd=mCourseTypes.end(); it!=itEnd; it++ ) {
			jsonItem->set( it->second.first, _fixStrSTL( it->second.second.GetAsString( wxC2S_HTML_SYNTAX ) ) );
		}
		json->set( COURSE_TYPE_COLOR, jsonItem );
	}
	{
		Poco::JSON::Object::Ptr jsonItem = new Poco::JSON::Object;
		typedef std::map < DayOfWeek, std::pair < std::string, bool > >::iterator It;
		for( It it=mDays.begin(), itEnd=mDays.end(); it!=itEnd; it++ ) {
			jsonItem->set( it->second.first, it->second.second );
		}
		json->set( DAY_VISIBILITY, jsonItem );
	}

	return json;
}

}
}
