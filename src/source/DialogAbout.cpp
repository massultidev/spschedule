/* Copyright by: P.J. Grochowski */

#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/colour.h>
#include <wx/dcclient.h>
#include <wx/gdicmn.h>

#include "gdev/guiWX/LangSwitcher.h"
#include "gdev/utility/Strings.h"
#include "gdev/utility/Resources.h"
#include "gdev/appWx/App.h"

#include "DialogAbout.h"

namespace gdev {
namespace spSchedule {

DialogAbout::DialogAbout(wxWindow* parent):
		wxDialog(
			parent,
			wxID_ANY,
			wxEmptyString,
			wxDefaultPosition,
			wxDefaultSize,
			wxDEFAULT_DIALOG_STYLE
		)
{
	// Set title:
	const std::string TITLE = "dialog_title_about";
	SetTitle(_getStrWX(TITLE));

	// Set icon:
	{
		wxIcon icon;
		icon.CopyFromBitmap(_getBitmapWX("appicon.png"));
		SetIcon(icon);
	}

	// Set dialog resizing hints:
	SetSizeHints(wxDefaultSize, wxDefaultSize);

	// Set background window color:
	SetForegroundColour(wxColour(0, 0, 0));
	SetBackgroundColour(wxColour(255, 255, 255));

	// Create layout:
	wxBoxSizer* layout = new wxBoxSizer(wxVERTICAL);

	// Create static bitmap control:
	wxStaticBitmap* ctrlImage = new wxStaticBitmap(this, wxID_ANY, _getBitmapWX( "about.png" ), wxDefaultPosition, wxDefaultSize, 0);
	layout->Add(ctrlImage, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5);

	// Create static text control for "Author":
	std::string strAuthor = std::string("by ") + gdev::appWx::App::getAuthor();
	wxStaticText* ctrlAuthor = new wxStaticText(this, wxID_ANY, _fixStrWX( strAuthor ), wxDefaultPosition, wxDefaultSize, 0);
	ctrlAuthor->Wrap( -1 );
	ctrlAuthor->Disable();
	layout->Add( ctrlAuthor, 0, wxALIGN_RIGHT | wxRIGHT | wxLEFT, 40 );

	// Create static text control for "Description":
	const std::string DIALOG_ABOUT_DESCRIPTION = "dialog_about_description";
	wxStaticText* ctrlDescription = new wxStaticText( this, wxID_ANY, _getStrWX( DIALOG_ABOUT_DESCRIPTION ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlDescription->Wrap( -1 );
	layout->Add( ctrlDescription, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5 );

	// Create static text control for "Version:
	std::string strAppVersion = std::string( "Ver." ) + gdev::appWx::App::getVersion();
	wxStaticText* ctrlVersion = new wxStaticText( this, wxID_ANY, _fixStrWX( strAppVersion ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlVersion->Wrap( -1 );
	ctrlVersion->Disable();
	layout->Add( ctrlVersion, 0, wxALIGN_RIGHT | wxRIGHT | wxLEFT, 30 );

	// Create button control for closing dialog:
	wxStdDialogButtonSizer* layoutBtn = new wxStdDialogButtonSizer();
	mCtrlBtnOk = new wxButton( this, wxID_OK );
	layoutBtn->AddButton( mCtrlBtnOk );
	layoutBtn->Realize();
	layout->Add( layoutBtn, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

	// Set layout:
	SetSizer( layout );
	Layout();
	layout->Fit( this );

	// Center dialog:
	Centre( wxBOTH );

	// Bind events:
	mCtrlBtnOk->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogAbout::onBtnOk, this );

	// Language switcher:
	gdev::guiWx::LangSwitcher::Ptr langSwitcher = gdev::guiWx::LangSwitcher::getInstance();
	langSwitcher->addWindow( this, TITLE );
	langSwitcher->addStaticText( ctrlDescription, DIALOG_ABOUT_DESCRIPTION );
}

DialogAbout::~DialogAbout() {
	mCtrlBtnOk->Unbind(wxEVT_COMMAND_BUTTON_CLICKED, &DialogAbout::onBtnOk, this);
}

void DialogAbout::show() {
	Centre( wxBOTH );
	Fit();
	ShowModal();
}

void DialogAbout::onBtnOk(wxCommandEvent&) {
	EndModal(true);
}

}
}
