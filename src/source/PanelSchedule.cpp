/* Copyright by: P.J. Grochowski */

#include "PanelSchedule.h"

#include "gdev/guiWX/CustomRefreshTrigger.h"
#include "gdev/guiWX/LangSwitcher.h"
#include "gdev/utility/Strings.h"

#include <wx/dcclient.h>
#include <wx/dcgraph.h>
#include <wx/menu.h>
#include <wx/msgdlg.h>

#include <Poco/Format.h>
#include <Poco/LineEndingConverter.h>

namespace gdev {
namespace spSchedule {

PanelSchedule::PanelSchedule(
		wxWindow* parent,
		DialogForm* dlgForm
):
		wxPanel( parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE|wxTAB_TRAVERSAL ),
		mSettings( Settings::getInstance() ),
		mCourses( Course::getCourses() ),
		mSchedulePainter( new SchedulePainter( this ) ),
		mDlgForm( dlgForm )
{
	SetForegroundColour( wxColour( 0, 0, 0 ) );
	SetBackgroundColour( wxColour( 255, 255, 255 ) );
	SetMinSize( wxSize( 800, 600 ) );

	gdev::guiWx::LangSwitcher::getInstance()->addCustomRefreshTrigger(
			new gdev::guiWx::CustomRefreshTrigger < PanelSchedule >( *this, &PanelSchedule::refresh )
	);

	// Bind events:
	Bind( wxEVT_PAINT, &PanelSchedule::onPaint, this );
	Bind( wxEVT_LEFT_UP, &PanelSchedule::onLMB, this );
	Bind( wxEVT_RIGHT_UP, &PanelSchedule::onRMB, this );
}

PanelSchedule::~PanelSchedule() {
	Unbind( wxEVT_PAINT, &PanelSchedule::onPaint, this );
	Unbind( wxEVT_LEFT_UP, &PanelSchedule::onLMB, this );
	Unbind( wxEVT_RIGHT_UP, &PanelSchedule::onRMB, this );
}

void PanelSchedule::onPaint( wxPaintEvent& ) {
	wxPaintDC pdc( this );
	wxGCDC dc( pdc );

	mSchedulePainter->drawBoard( dc );
}
void PanelSchedule::onLMB( wxMouseEvent& event ) {
	SetFocus();
	SetFocusFromKbd();
	typedef std::vector < Course::Ptr >::reverse_iterator It;
	for( It it=mCourses->rbegin(), itEnd=mCourses->rend(); it!=itEnd; it++ )
	{
		Poco::SharedPtr < wxRect > courseRectPtr = mSchedulePainter->getCourseRect( *it );
		if( courseRectPtr.isNull() || !courseRectPtr->Contains( event.GetPosition() ) ) {
			continue;
		}
		if( it != mCourses->rbegin() )
		{
			Course::Ptr course = *it;
			mCourses->erase( (++it).base() );
			mCourses->push_back( course );
			Refresh();
		}
		return;
	}
}
void PanelSchedule::onRMB( wxMouseEvent& event ) {
	SetFocus();
	SetFocusFromKbd();
	typedef std::vector < Course::Ptr >::reverse_iterator It;
	for( It it=mCourses->rbegin(), itEnd=mCourses->rend(); it!=itEnd; it++ )
	{
		Poco::SharedPtr < wxRect > courseRectPtr = mSchedulePainter->getCourseRect( *it );
		if( courseRectPtr.isNull() || !courseRectPtr->Contains( event.GetPosition() ) ) {
			continue;
		}

		enum PopupMenuID {
			ID_EDIT,
			ID_DELETE,
			ID_DUPLICATE,
			ID_CANCEL
		};
		wxMenu menu;
		menu.Append( ID_EDIT, _getStrWX( "popupmenu_edit" ) );
		menu.Append( ID_DELETE, _getStrWX( "popupmenu_delete" ) );
		menu.Append( ID_DUPLICATE, _getStrWX( "popupmenu_duplicate" ) );
		menu.Append( ID_CANCEL, _getStrWX( "popupmenu_cancel" ) );

		switch( GetPopupMenuSelectionFromUser( menu, ScreenToClient( ClientToScreen( event.GetPosition() ) ) ) )
		{
			case (int)ID_EDIT: {
				Course::Ptr course = *it;
				if( mDlgForm->show( course ) ) {
					refresh();
				}
			} break;
			case (int)ID_DUPLICATE: {
				Course::Ptr course = (*it)->clone();
				if( mDlgForm->show( course ) ) {
					mCourses->push_back( course );
					refresh();
				}
			} break;
			case (int)ID_DELETE: {
				if( wxMessageBox(
						_getStrWX( "msgbox_question_delete_course" ),
						_getStrWX( "question" ),
						wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
					) == wxYES )
				{
					mCourses->erase( (++it).base() );
					refresh();
				}
			} break;
		}
		return;
	}
}

void PanelSchedule::refresh() {
	Refresh();
}

}
}
