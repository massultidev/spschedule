/* Copyright by: P.J. Grochowski */

#include "SchedulePainter.h"

#include "gdev/utility/Strings.h"
#include "gdev/appWx/App.h"

#include <Poco/Format.h>
#include <Poco/LineEndingConverter.h>

#include <math.h>

namespace gdev {
namespace spSchedule {

SchedulePainter::SchedulePainter( wxWindow* parent ):
		mSettings( Settings::getInstance() ),
		mCourses( Course::getCourses() ),
		mParent( parent ),
		mSize( wxSize( 0, 0 ) ),
		mIsImagePainter( false )
{}
SchedulePainter::SchedulePainter( const wxSize& size ):
		mSettings( Settings::getInstance() ),
		mCourses( Course::getCourses() ),
		mParent( NULL ),
		mSize( size ),
		mIsImagePainter( true )
{}
SchedulePainter::~SchedulePainter() {
}

void SchedulePainter::drawBoard( wxGCDC& dc ) const
{
	SchedulePainterParameters::Ptr params = getParameters();

	const int WIDTH 	= params->getWidth();
	const int HEIGHT 	= params->getHeight();

	const int SCALE_FACTOR = params->getScaleFactor();

	const int OFFSET_FRAME_X = params->getOffsetFrameX();
	const int OFFSET_FRAME_Y = params->getOffsetFrameY();

	const int BOARD_X_POS 	= params->getBoardXPos();
	const int BOARD_Y_POS 	= params->getBoardYPos();
	const int BOARD_WIDTH 	= params->getBoardWidth();
	const int BOARD_HEIGHT 	= params->getBoardHeight();

	std::pair < int, int > rangeHour = mSettings->getRangeHours();
	const int HOUR_COUNT 	= params->getHourCount();
	const int HOUR_HEIGHT 	= params->getHourHeight();
	const int DAY_WIDTH 	= params->getDayWidth();

	const int OFFSET_LABEL_HOUR = OFFSET_FRAME_X/20;
	const int OFFSET_LABEL_DAY_H = OFFSET_FRAME_Y/4;
	const int OFFSET_LABEL_DAY_V = OFFSET_FRAME_Y/8;

	wxColour colorGray( 160, 160, 160 );
	wxPen penGray( colorGray, 1, wxPENSTYLE_SOLID );
	wxBrush brushGray( colorGray );
	wxFont fontSlim = dc.GetFont();
	fontSlim.SetFamily( wxFONTFAMILY_SWISS );
	wxFont fontBold = fontSlim.Bold();
	fontSlim.SetPointSize( (int)((HEIGHT*1.38)/100) );
	fontBold.SetPointSize( (int)((HEIGHT*1.55)/100) );
	dc.SetFont( fontBold );

	std::map < Settings::DayOfWeek, std::string > dayLabels = mSettings->getDaysLabels();
	std::map < Settings::DayOfWeek, bool > dayVisibility = mSettings->getDaysVisibility();

	if( mIsImagePainter ) {
		dc.SetBackground( *wxWHITE_BRUSH );
		dc.Clear();
	}

	std::size_t sizeLine = ceil((HEIGHT*0.125)/100);

	dc.SetPen( penGray );
	dc.SetBrush( brushGray );
	for( int i=0, pos=0; i<HOUR_COUNT+1; i++, pos+=HOUR_HEIGHT )
	{
		if( pos > 0 ) {
			//dc.DrawLine( BOARD_X_POS, BOARD_Y_POS+pos, (mIsImagePainter ? (WIDTH-OFFSET_FRAME_X-1) : WIDTH), BOARD_Y_POS+pos );
			drawThickLine( dc, BOARD_X_POS, BOARD_Y_POS+pos, (mIsImagePainter ? (WIDTH-OFFSET_FRAME_X-1) : WIDTH), BOARD_Y_POS+pos, sizeLine );
		}
		if( !(i<HOUR_COUNT) && mIsImagePainter ) {
			continue;
		}
	}
	for( size_t i=0, pos=BOARD_X_POS+DAY_WIDTH; i<dayLabels.size(); i++ )
	{
		if( !dayVisibility[(Settings::DayOfWeek)i] ) {
			continue;
		}
		//dc.DrawLine( BOARD_X_POS+pos, BOARD_Y_POS, BOARD_X_POS+pos, (mIsImagePainter ? (HEIGHT-OFFSET_FRAME_Y-1) : HEIGHT) );
		drawThickLine( dc, pos, BOARD_Y_POS, pos, (mIsImagePainter ? (HEIGHT-OFFSET_FRAME_Y-1) : HEIGHT), sizeLine );
		pos+=DAY_WIDTH;
	}

	//dc.SetPen( penBlack );
	dc.SetFont( fontSlim );
	int radius = ((SCALE_FACTOR*0.143)/100)*mSettings->getEdgesRadius();
	typedef std::vector < Course::Ptr >::const_iterator It;
	for( It it=mCourses->begin(), itEnd=mCourses->end(); it!=itEnd; it++ )
	{
		Poco::SharedPtr < wxRect > courseRectPtr = getCourseRectInternal( *it, params );
		if( courseRectPtr.isNull() ) {
			continue;
		}
		dc.SetPen( *wxBLACK_PEN );
		dc.SetBrush( *wxBLACK_BRUSH );
		dc.DrawRoundedRectangle( *courseRectPtr, radius );

		wxColour color = mSettings->getCourseTypeColour( (*it)->getType() );
		dc.SetPen( wxPen( color ) );
		dc.SetBrush( wxBrush( color ) );
		wxRect innerRect = *courseRectPtr;
		innerRect.Deflate( (HEIGHT*0.2)/100 );
		dc.DrawRoundedRectangle( innerRect, radius );

		//courseRectPtr->Deflate( (WIDTH*1)/100, (HEIGHT*0.7)/100 );
		courseRectPtr->Deflate( (WIDTH*0.8)/100, (HEIGHT*0.75)/100 );
		int maxLinesCount = courseRectPtr->GetHeight()/dc.GetCharHeight();
		wxString courseDetailsStrWX;
		std::vector < std::string > lines;
		{
			std::string place = (*it)->getPlace();
			if( !place.empty() ) {
				lines.push_back( place );
			}
			std::string subject = (*it)->getSubject();
			if( !subject.empty() ) {
				lines.push_back( subject );
			}
			std::string tutor = (*it)->getTutor();
			if( !tutor.empty() ) {
				lines.push_back( tutor );
			}
		}
		for( int i=0; i<maxLinesCount; i++ )
		{
			if( i==0 ) {
				courseDetailsStrWX += fitString( dc, _fixStrWX( (*it)->getTimeStr() ), *courseRectPtr );
				continue;
			}
			if( (long)i>(long)lines.size() ) {
				break;
			}
			courseDetailsStrWX += Poco::LineEnding::NEWLINE_DEFAULT + fitString( dc, _fixStrWX( lines.at(i-1) ), *courseRectPtr, true );
		}
		dc.DrawLabel(
				courseDetailsStrWX,
				*courseRectPtr,
				wxALIGN_LEFT|wxALIGN_TOP
		);
	}

	dc.SetPen( *wxWHITE_PEN );
	dc.SetBrush( *wxWHITE_BRUSH );
	if( mIsImagePainter )
	{
		dc.DrawRectangle( 0, 0, WIDTH, BOARD_Y_POS );
		dc.DrawRectangle( 0, 0, BOARD_X_POS, HEIGHT );
		dc.DrawRectangle( WIDTH-BOARD_X_POS, 0, BOARD_X_POS, HEIGHT );
		dc.DrawRectangle( 0, HEIGHT-BOARD_Y_POS, WIDTH, BOARD_Y_POS );
	} else {
		dc.DrawRectangle( 0, 0, BOARD_WIDTH+BOARD_X_POS, BOARD_Y_POS );
		dc.DrawRectangle( 0, BOARD_Y_POS, BOARD_X_POS, BOARD_HEIGHT+BOARD_Y_POS );
	}
	dc.SetPen( *wxBLACK_PEN );
	dc.SetBrush( *wxBLACK_BRUSH );
	if( mIsImagePainter )
	{
		//dc.DrawRectangle( BOARD_X_POS, BOARD_Y_POS, BOARD_WIDTH, BOARD_HEIGHT );
		drawThickLine( dc, BOARD_X_POS, BOARD_Y_POS, BOARD_X_POS+BOARD_WIDTH, BOARD_Y_POS, sizeLine );
		drawThickLine( dc, BOARD_X_POS, BOARD_Y_POS, BOARD_X_POS, BOARD_Y_POS+BOARD_HEIGHT, sizeLine );
		drawThickLine( dc, BOARD_X_POS+BOARD_WIDTH, BOARD_Y_POS, BOARD_X_POS+BOARD_WIDTH, BOARD_Y_POS+BOARD_HEIGHT, sizeLine );
		drawThickLine( dc, BOARD_X_POS, BOARD_Y_POS+BOARD_HEIGHT, BOARD_X_POS+BOARD_WIDTH, BOARD_Y_POS+BOARD_HEIGHT, sizeLine );
	} else {
		//dc.DrawLine( BOARD_X_POS, 0, BOARD_X_POS, HEIGHT );
		//dc.DrawLine( 0, BOARD_Y_POS, WIDTH, BOARD_Y_POS );
		drawThickLine( dc, BOARD_X_POS, 0, BOARD_X_POS, HEIGHT, sizeLine );
		drawThickLine( dc, 0, BOARD_Y_POS, WIDTH, BOARD_Y_POS, sizeLine );
		for( int i=0, pos=HOUR_HEIGHT; i<HOUR_COUNT; i++, pos+=HOUR_HEIGHT) {
			//dc.DrawLine( 0, BOARD_Y_POS+pos, BOARD_X_POS, BOARD_Y_POS+pos );
			drawThickLine( dc, 0, BOARD_Y_POS+pos, BOARD_X_POS, BOARD_Y_POS+pos, sizeLine );
		}
		for( size_t i=0, pos=BOARD_X_POS+DAY_WIDTH; i<dayLabels.size(); i++ )
		{
			if( !dayVisibility[(Settings::DayOfWeek)i] ) {
				continue;
			}
			//dc.DrawLine( BOARD_X_POS+pos, 0, BOARD_X_POS+pos, BOARD_Y_POS );
			drawThickLine( dc, pos, 0, pos, BOARD_Y_POS, sizeLine );
			pos+=DAY_WIDTH;
		}
	}

	dc.SetFont( fontBold );
	for( int i=0, pos=0; i<HOUR_COUNT+1; i++, pos+=HOUR_HEIGHT )
	{
		if( !(i<HOUR_COUNT) && mIsImagePainter ) {
			continue;
		}
		dc.DrawLabel(
				_fixStrWX( Poco::format( "%?d", rangeHour.first + i ) ),
				wxRect( wxPoint( OFFSET_LABEL_HOUR, BOARD_Y_POS+pos+OFFSET_LABEL_HOUR ), wxSize( BOARD_X_POS-2*OFFSET_LABEL_HOUR, HOUR_HEIGHT-2*OFFSET_LABEL_HOUR ) ),
				wxALIGN_TOP|wxALIGN_RIGHT
		);
	}
	for( size_t i=0, pos=0; i<dayLabels.size(); i++ )
	{
		if( !dayVisibility[(Settings::DayOfWeek)i] ) {
			continue;
		}
		dc.DrawLabel(
				_getStrWX( dayLabels.at((Settings::DayOfWeek)i) ),
				wxRect( wxPoint( BOARD_X_POS+pos+OFFSET_LABEL_DAY_H, OFFSET_LABEL_DAY_V ), wxSize( DAY_WIDTH-2*OFFSET_LABEL_DAY_H, BOARD_Y_POS-2*OFFSET_LABEL_DAY_V ) ),
				(mIsImagePainter ? wxALIGN_CENTER : wxALIGN_LEFT)|wxALIGN_BOTTOM
		);
		pos+=DAY_WIDTH;
	}
	if( mIsImagePainter ) {
		dc.SetFont( fontSlim );
		std::map < Settings::CourseType, std::string > courseTypeLabels = mSettings->getCourseTypeLabels();
		const int extraOffset = BOARD_X_POS/10;
		const int posOffest = BOARD_X_POS/4 + extraOffset*2;
		const int yPos = BOARD_Y_POS + BOARD_HEIGHT + ((BOARD_Y_POS-dc.GetCharHeight())/2);
		int xPos = BOARD_X_POS + (extraOffset*2);
		typedef std::map < Settings::CourseType, std::string >::iterator It;
		for( It it=courseTypeLabels.begin(), itEnd=courseTypeLabels.end(); it!=itEnd; it++ ) {
			wxString lbl = _getStrWX( it->second + "_capital" );
			wxSize lblSize = dc.GetTextExtent( lbl );
			wxRect lblRect( xPos, yPos, lblSize.GetWidth(), lblSize.GetHeight() );
			wxColour lblColor = mSettings->getCourseTypeColour( it->first );

			dc.SetBrush( wxBrush( lblColor ) );
			dc.SetPen( wxPen( lblColor ) );
			dc.DrawRectangle( wxRect( lblRect ).Inflate( (extraOffset*2), extraOffset ) );
			dc.DrawLabel( lbl, lblRect, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL );

			xPos += lblSize.GetWidth() + posOffest;
		}
		dc.DrawLabel(
				_fixStrWX(std::string(gdev::appWx::App::getName() + " v." + gdev::appWx::App::getVersion() + " by " + gdev::appWx::App::getAuthor())),
				wxRect( BOARD_X_POS + BOARD_WIDTH/2, BOARD_Y_POS + BOARD_HEIGHT, BOARD_WIDTH/2, BOARD_Y_POS ),
				wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT
		);
	}
}

Poco::SharedPtr < wxRect > SchedulePainter::getCourseRect( Course::Ptr course ) const {
	return getCourseRectInternal( course, NULL );
}

SchedulePainterParameters::Ptr SchedulePainter::getParameters() const {
	return new SchedulePainterParameters( (mIsImagePainter ? mSize : mParent->GetSize()), mIsImagePainter );
}

Poco::SharedPtr < wxRect > SchedulePainter::getCourseRectInternal( Course::Ptr course, SchedulePainterParameters::Ptr params ) const
{
	if( params.isNull() ) {
		params = getParameters();
	}

	const int BOARD_X_POS 	= params->getBoardXPos();
	const int BOARD_Y_POS 	= params->getBoardYPos();

	std::pair < int, int > rangeHour = mSettings->getRangeHours();
	const int HOUR_HEIGHT 	= params->getHourHeight();
	const int DAY_WIDTH 	= params->getDayWidth();

	Settings::WeekCycle courseWeekCycle = course->getWeekCycle();
	Settings::DayOfWeek dayOfWeek = course->getDay();
	int x = BOARD_X_POS;
	std::map < Settings::DayOfWeek, bool > dayVisibility = mSettings->getDaysVisibility();
	if( !dayVisibility[dayOfWeek] ) {
		return NULL;
	}
	typedef std::map < Settings::DayOfWeek, bool >::const_iterator It;
	for( It it=dayVisibility.begin(), itEnd=dayVisibility.end(); it!=itEnd; it++ )
	{
		if( dayOfWeek == it->first ) {
			break;
		}
		if( it->second ) {
			x += DAY_WIDTH;
		}
	}
	int w = DAY_WIDTH;
	switch( (int)courseWeekCycle )
	{
		case (int)Settings::ODD: {
			w = DAY_WIDTH/2;
		} break;
		case (int)Settings::EVEN: {
			int offset = DAY_WIDTH/2;
			x += offset;
			w = DAY_WIDTH-offset;
		}
	}
	Poco::Tuple < int, int, int, int > time = course->getTime();
	int startHour 	= time.get<0>();
	int startMinute = time.get<1>();
	int endHour 	= time.get<2>();
	int endMinute 	= time.get<3>();
	int quarter = HOUR_HEIGHT/4;
	int y = ( ( startHour - rangeHour.first ) * HOUR_HEIGHT ) + ( quarter * quartersCount( startMinute ) );
	int h = ( ( ( endHour - rangeHour.first ) * HOUR_HEIGHT ) + ( quarter * quartersCount( endMinute ) ) ) - y;
	y += BOARD_Y_POS;

	return new wxRect( x, y, w, h );
}

void SchedulePainter::drawThickLine( wxGCDC& dc, int x1, int y1, int x2, int y2, int size ) const
{
	if( size < 2 ) {
		#ifdef GDEV_WINDOWS
			dc.DrawLine( x1, y1, x2, y2 );
		#else
				dc.DrawLine( x1, y1, (x1==x2 ? x2 : x2+1), (y1==y2 ? y2 : y2+1) );
		#endif
		return;
	}
	if( x1 == x2 ) {
		if( y1 == y2 ) {
			dc.DrawPoint( x1, x2 );
			return;
		}
		dc.DrawRectangle( x1, y1, size, y2-y1+size );
		return;
	}
	if( y1 == y2 ) {
		dc.DrawRectangle( x1, y1, x2-x1+size, size );
		return;
	}
	#ifdef GDEV_DEBUG
		throw Poco::Exception( "Thick vertical lines are not yet supported!" ); // TODO: Add vertical lines support!
	#endif
	dc.DrawLine( x1, y1, x2, y2 );
}

int SchedulePainter::quartersCount( int minutes ) const
{
	if( minutes < 15 ) {
		return 0;
	}
	if( ( minutes >= 15 ) && ( minutes < 30 ) ) {
		return 1;
	}
	if( ( minutes >= 30 ) && ( minutes < 45 ) ) {
		return 2;
	}
	return 3;
}

wxString SchedulePainter::fitString( const wxGCDC& dc, const wxString& str, const wxRect& rect, bool ellipsize ) const
{
	if( str.IsEmpty() ) {
		return str;
	}

	std::size_t i=0;
	while( dc.GetTextExtent( str.Mid( 0, str.size()-i ) ).GetWidth() > rect.GetWidth() ) {
		i++;
	}
	wxString nStr = str.Mid( 0, str.size()-i );

	if( (i == 0) || (!ellipsize) ) {
		return nStr;
	}

	const std::string ELLIPSIS = "...";

	return nStr.Mid( 0, nStr.size()-ELLIPSIS.size() ) + ELLIPSIS;
}

}
}
