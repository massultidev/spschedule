/* Copyright by: P.J. Grochowski */

#include "SPSchedule.h"

#include "gdev/utility/Strings.h"

#include <wx/intl.h>
#include <wx/msgdlg.h>
#include <wx/gdicmn.h>
#include <wx/stdpaths.h>

#include <Poco/Format.h>
#include <Poco/Path.h>
#include <Poco/File.h>
#include <Poco/Timespan.h>
#include <Poco/Timestamp.h>
#include <Poco/DateTimeFormatter.h>

#include <string>

namespace gdev {
namespace spSchedule {

SPSchedule::SPSchedule() {
	mSplash = NULL;
	mFrame = NULL;
}
SPSchedule::~SPSchedule() {
}

bool SPSchedule::init() {
	#if _IS_LINUX
		// Important for command line arguments!
		if( !wxApp::OnInit() ) {
			return false;
		}
	#endif

	mSettings = Settings::getInstance();
	mSettings->load();
	mFrame = new Frame();
	mSplash = new gdev::guiWx::SplashScreen(mFrame);

	return true;
}

#if _IS_LINUX
	static const std::string CMD_OPT_HELP = "help";
	static const std::string CMD_OPT_ICON = "icon";

	void SPSchedule::OnInitCmdLine( wxCmdLineParser& parser ) {
		static const char cmdOptHelp[] = { CMD_OPT_HELP[0], '\0' };
		static const char cmdOptIcon[] = { CMD_OPT_ICON[0], '\0' };
		const wxCmdLineEntryDesc cmdLineEntryDesc[] = {
				{ wxCMD_LINE_SWITCH, cmdOptHelp, CMD_OPT_HELP.c_str(), "display help", wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
				{ wxCMD_LINE_SWITCH, cmdOptIcon, CMD_OPT_ICON.c_str(), "extract icon", wxCMD_LINE_VAL_NONE, wxCMD_LINE_PARAM_OPTIONAL },
				wxCMD_LINE_DESC_END
		};
		parser.SetDesc( cmdLineEntryDesc );
		parser.SetSwitchChars( L"-" );
		parser.SetLogo( Poco::format( "%s - Application for student timetable creation.\n", getName() ) );
		parser.AddUsageText(
				"\n"
				"It is a window (gui) application.\n"
				"This command line interface is available only in Linux.\n"
				"Furthermore it exists only for easy restore of icon file of the application."
		);
	}

	bool SPSchedule::OnCmdLineParsed( wxCmdLineParser& parser ) {
		if( parser.Found( CMD_OPT_ICON ) ) {
			std::string icoPath = Poco::Path( _fixStrSTL( wxStandardPaths::Get().GetExecutablePath() ) )
				.setFileName(getName() + ".png").toString();
			_getBitmapWX( "appicon.png" ).SaveFile( _fixStrWX( icoPath ), wxBITMAP_TYPE_PNG );
			std::cout<<Poco::format( "Icon extracted to:\n'%s'", icoPath )<<std::endl; // Use in release too!
			exit( 0 );
		}

		return true;
	}
	bool SPSchedule::OnCmdLineHelp( wxCmdLineParser& parser ) {
		parser.Usage();
		exit( 0 );
	}
	bool SPSchedule::OnCmdLineError( wxCmdLineParser& parser ) {
		parser.Usage();
		exit( 1 );
	}
#endif

}
}
