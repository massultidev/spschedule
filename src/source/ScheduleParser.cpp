/* Copyright by: P.J. Grochowski */

#include "ScheduleParser.h"

#include "Settings.h"

#include <Poco/Format.h>
#include <Poco/NumberParser.h>
#include <Poco/DateTime.h>
#include <Poco/Timezone.h>
#include <Poco/DateTimeFormatter.h>

#include <set>

namespace gdev {
namespace spSchedule {

ScheduleParser::ScheduleParser() {
}

std::vector < Course::Ptr > ScheduleParser::parseICalEvents( std::vector < gdev::iCal::VEvent::Ptr > iCalEvents )
{
    if( !iCalEvents.size() ) {
        throw Poco::Exception( "No events to parse!" );
    }

	// Check if required keys are present:
	std::vector < std::string > reqKeys;
	reqKeys.push_back( gdev::iCal::VEvent::KEY_DTSTART );
	reqKeys.push_back( gdev::iCal::VEvent::KEY_DTEND );
	reqKeys.push_back( gdev::iCal::VEvent::KEY_SUMMARY );
	reqKeys.push_back( gdev::iCal::VEvent::KEY_DESCRIPTION );
	reqKeys.push_back( gdev::iCal::VEvent::KEY_LOCATION );
	for( std::size_t i=0; i<iCalEvents.size(); i++ )
	{
		// Check if all present:
		for( std::size_t k=0; k<reqKeys.size(); k++ )
		{
			// Check if key present:
			if( !iCalEvents.at( i )->hasKey( reqKeys.at( k ) ) ) {
				throw Poco::Exception( Poco::format( "Event '%?d' is missing '%s' key!", i, reqKeys.at(k) ) );
			}
		}
	}

	// Verify time keys correctness:
	for( std::size_t i=0; i<iCalEvents.size(); i++ )
	{
		// Check both time stamps:
		for( std::size_t k=0; k<2; k++ )
		{
			// Get time stamp value:
			std::string strDT = iCalEvents.at( i )->getValue( reqKeys.at( k ) );

			// Check if size correct:
			if( strDT.size() != 16 ) {
				throw Poco::Exception( Poco::format( "Event '%?d' at key '%s' must be 16 letters long!", i, reqKeys.at(k) ) );
			}

			// Check if letters present:
			static const char c[] = { 'T', 'Z' };
			for( std::size_t l=8, cidx=0; l<16; l+=7, cidx++ )
			{
				if( strDT.at( l ) != c[ cidx ] ) {
					throw Poco::Exception( Poco::format( "Event '%?d' at key '%s' is missing letter '%c' in timestamp!", i, reqKeys.at(k), c[cidx] ) );
				}
			}

			// Check if numbers present:
			for( std::size_t p=0; p<8; p++ )
			{
				if( !std::isdigit( strDT.at( p ) ) ) {
					throw Poco::Exception( Poco::format( "Event '%?d' at key '%s' must have a timestamp!", i, reqKeys.at(k) ) );
				}
			}
			for( std::size_t p=9; p<15; p++ )
			{
				if( !std::isdigit( strDT.at( p ) ) ) {
					throw Poco::Exception( Poco::format( "Event '%?d' at key '%s' must have a timestamp!", i, reqKeys.at(k) ) );
				}
			}
		}
	}

	// Class names:
	std::vector < std::string > clName;
	std::map < std::string, std::pair < std::string, std::string > > locationAndLecturer;
	for( std::size_t i=0; i<iCalEvents.size(); i++ )
	{
		std::string strInstance = iCalEvents.at( i )->getValue( gdev::iCal::VEvent::KEY_SUMMARY );
		clName.push_back( strInstance );
		if( !locationAndLecturer.count( strInstance ) )
		{
			std::string strLocation = iCalEvents.at( i )->getValue( gdev::iCal::VEvent::KEY_LOCATION );
			std::string strLecturer = iCalEvents.at( i )->getValue( gdev::iCal::VEvent::KEY_DESCRIPTION );
			locationAndLecturer[ strInstance ] =
					std::pair < std::string, std::string > ( strLocation, strLecturer );
		}
	}
	std::sort( clName.begin(), clName.end() );
	clName.erase( unique( clName.begin(), clName.end() ), clName.end() );

	// Get date and time:
	std::vector < std::vector < std::pair < std::string, std::string > > > dtPairsVectors( clName.size() );
	std::vector < std::vector < std::string > > dtWeekCycle( clName.size() );
	for( std::size_t i=0; i<iCalEvents.size(); i++ )
	{
		// Get name:
		std::string strName = iCalEvents.at( i )->getValue( gdev::iCal::VEvent::KEY_SUMMARY );

		// Check for what name:
		for( std::size_t k=0; k<clName.size(); k++ )
		{
			if( strName.compare( clName.at( k ) ) && ( k == clName.size()-1 ) ) {
				throw Poco::Exception( Poco::format( "Event '%?d' does not belong to any of detected class types!", i ) );
			} else
			if( !strName.compare( clName.at( k ) ) )
			{
				// Create pair:
				std::vector < std::string > sKey;
				sKey.push_back( gdev::iCal::VEvent::KEY_DTSTART );
				sKey.push_back( gdev::iCal::VEvent::KEY_DTEND );
				std::vector < std::string > sDT;
				sDT.push_back( iCalEvents.at( i )->getValue( sKey.at( 0 ) ) );
				sDT.push_back( iCalEvents.at( i )->getValue( sKey.at( 1 ) ) );

				// Get year month and day for week cycle:
				dtWeekCycle.at( k ).push_back( sDT.at( 0 ).substr( 0, 8 ) );

				// Check if date requires conversion to local time:
				for( std::size_t s=0; s<2; s++ )
				{
					if( iCalEvents.at( i )->getParam( sKey.at( s ) ).find( gdev::iCal::VEvent::PARAM_TZID ) != 0 )
					{
						Poco::DateTime dt(
								Poco::NumberParser::parse( sDT.at( s ).substr( 0, 4 ) ),
								Poco::NumberParser::parse( sDT.at( s ).substr( 4, 2 ) ),
								Poco::NumberParser::parse( sDT.at( s ).substr( 6, 2 ) ),
								Poco::NumberParser::parse( sDT.at( s ).substr( 9, 2 ) ),
								Poco::NumberParser::parse( sDT.at( s ).substr( 11, 2 ) )
						);
						dt.makeLocal( Poco::Timezone::tzd() );
						sDT[ s ] = Poco::DateTimeFormatter::format( dt, "%Y%m%dT%H%M%SZ", Poco::Timezone::tzd() );
					}
				}

				// Add to pairs vector:
				dtPairsVectors.at( k ).push_back( std::make_pair( sDT.at( 0 ), sDT.at( 1 ) ) );

				// Escape loop:
				break;
			}
		}
	}

    // Get time of start and end:
	std::vector < std::pair < std::string, std::string > > oneDTVector;
	for( std::size_t i=0; i<dtPairsVectors.size(); i++ )
	{
		// Create set from single type vector:
		std::vector < std::string > dtStart;
		std::vector < std::string > dtStop;

		for( std::size_t k=0; k<dtPairsVectors.at( i ).size(); k++ )
		{
			dtStart.push_back( dtPairsVectors.at( i ).at( k ).first.substr( 9, 4 ) );
			dtStop.push_back( dtPairsVectors.at( i ).at( k ).second.substr( 9, 4 ) );
		}
		std::set < std::string > startSet( dtStart.begin(), dtStart.end() );
		std::set < std::string > stopSet( dtStop.begin(), dtStop.end() );

		// Count occurrences:
		std::vector < std::pair < std::string, std::size_t > > DT1;
		for( std::set < std::string >::iterator
				it = startSet.begin(), itEnd = startSet.end(); it != itEnd; it++ )
		{
			std::string str = *it;
			std::size_t c = std::count( dtStart.begin(), dtStart.end(), str );
			DT1.push_back( std::pair < std::string, std::size_t > ( str, c ) );
		}
		std::sort( DT1.begin(), DT1.end(), ScheduleParser::sortPair );
		std::vector < std::pair < std::string, std::size_t > > DT2;
		for( std::set < std::string >::iterator
				it = stopSet.begin(), itEnd = stopSet.end(); it != itEnd; it++ )
		{
			std::string str = *it;
			std::size_t c = std::count( dtStop.begin(), dtStop.end(), str );
			DT2.push_back( std::pair < std::string, std::size_t > ( str, c ) );
		}
		std::sort( DT2.begin(), DT2.end(), ScheduleParser::sortPair );

		// Store result:
		oneDTVector.push_back( std::pair < std::string, std::string > ( DT1.at( 0 ).first, DT2.at( 0 ).first ) );
	}

	// Get week cycle constant:
	std::vector < Settings::WeekCycle > weekCycle;
	for( std::size_t i=0; i<dtWeekCycle.size(); i++ )
	{
		std::size_t wTP = 0;
		std::size_t wTN = 0;
		for( std::size_t k=0; k<dtWeekCycle.at( i ).size(); k++ )
		{
			int year = Poco::NumberParser::parse( dtWeekCycle.at( i ).at( k ).substr( 0, 4 ) );
			Poco::DateTime dt(
					year,
					Poco::NumberParser::parse( dtWeekCycle.at( i ).at( k ).substr( 4, 2 ) ),
					Poco::NumberParser::parse( dtWeekCycle.at( i ).at( k ).substr( 6, 2 ) )
			);
			if( ( dt.week() + (int)(!Poco::DateTime(year,1,1).week()) )%2 )
			{
				wTN++;
			} else {
				wTP++;
			}
		}

		if( std::abs( static_cast<long>(wTN) - static_cast<long>(wTP) ) < 2 )
		{
			weekCycle.push_back( Settings::BOTH );
		} else
		if( wTP < wTN )
		{
			weekCycle.push_back( Settings::ODD );
		} else {
			weekCycle.push_back( Settings::EVEN );
		}
	}

	// Get day of week:
	std::vector < Settings::DayOfWeek > dayOfWeek;
	for( std::size_t i=0; i<dtWeekCycle.size(); i++ )
	{
		Poco::DateTime dt(
				Poco::NumberParser::parse( dtWeekCycle.at( i ).at( 0 ).substr( 0, 4 ) ),
				Poco::NumberParser::parse( dtWeekCycle.at( i ).at( 0 ).substr( 4, 2 ) ),
				Poco::NumberParser::parse( dtWeekCycle.at( i ).at( 0 ).substr( 6, 2 ) )
		);
		int dOW = dt.dayOfWeek();
		dOW = ( dOW == 0 ) ? 7 : dOW;
		dOW--;
		dayOfWeek.push_back( (Settings::DayOfWeek)dOW );
	}

	// Get schedule event types:
	std::vector < Settings::CourseType > eType;
	for( std::size_t i=0; i<clName.size(); i++ )
	{
		std::string sType = clName.at( i ).substr( 0, 1 );
		if( !sType.compare( "W" ) )	{ eType.push_back( Settings::LECTURE ); }
		else
		if( !sType.compare( "L" ) )	{ eType.push_back( Settings::LABORATORY ); }
		else
		if( !sType.compare( "C" ) )	{ eType.push_back( Settings::EXERCISES ); }
		else
		if( !sType.compare( "P" ) )	{ eType.push_back( Settings::PROJECT ); }
		else
		if( !sType.compare( "S" ) )	{ eType.push_back( Settings::SEMINAR ); }
		else
		{ eType.push_back( Settings::OTHER ); }
	}

	std::size_t clCount = clName.size();
	if( oneDTVector.size() != clCount )	{ throw Poco::Exception( "Internal error: [DTAM]" ); }
	if( weekCycle.size() != clCount )	{ throw Poco::Exception( "Internal error: [WCAM]" ); }
	if( eType.size() != clCount )		{ throw Poco::Exception( "Internal error: [ETAM]" ); }

	// Create events:
	std::vector < Course::Ptr > sEVec;
	for( std::size_t i=0; i<clName.size(); i++ )
	{
		sEVec.push_back( new Course(
				clName.at( i ).substr( 2 ),
				locationAndLecturer[ clName.at( i ) ].second,
				locationAndLecturer[ clName.at( i ) ].first,
				eType.at( i ),
				dayOfWeek.at( i ),
				weekCycle.at( i ),
				Poco::NumberParser::parse( oneDTVector.at( i ).first.substr( 0, 2 ) ),
				Poco::NumberParser::parse( oneDTVector.at( i ).first.substr( 2, 2 ) ),
				Poco::NumberParser::parse( oneDTVector.at( i ).second.substr( 0, 2 ) ),
				Poco::NumberParser::parse( oneDTVector.at( i ).second.substr( 2, 2 ) )
		));
	}
	return sEVec;
}

bool ScheduleParser::sortPair(
		const std::pair < std::string, std::size_t >& left,
		const std::pair < std::string, std::size_t >& right
)
{
	return left.second > right.second;
}

}
}
