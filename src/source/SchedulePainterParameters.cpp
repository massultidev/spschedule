/* Copyright by: P.J. Grochowski */

#include "SchedulePainterParameters.h"

namespace gdev {
namespace spSchedule {

SchedulePainterParameters::SchedulePainterParameters(
		wxSize size,
		bool isImagePainter
):
		mSettings( Settings::getInstance() )
{
	mWidth 	= size.GetWidth();
	mHeight = size.GetHeight();

	mScaleFactor = (mWidth+mHeight)/2;

	mOffsetFrameX = (isImagePainter ? (mScaleFactor/35) : ((mScaleFactor*2.86)/100));
	mOffsetFrameY = (isImagePainter ? mOffsetFrameX : ((mScaleFactor*3)/100));

	mBoardXPos 		= mOffsetFrameX;
	mBoardYPos 		= mOffsetFrameY;
	mBoardWidth 	= mWidth - (isImagePainter ? mBoardXPos*2 : mBoardXPos );
	mBoardHeight 	= mHeight - (isImagePainter ? mBoardYPos*2 : mBoardYPos );

	std::pair < int, int > rangeHour = mSettings->getRangeHours();
	mHourCount = rangeHour.second - rangeHour.first;
	mHourHeight = mBoardHeight/mHourCount;
	mDayWidth = mBoardWidth/mSettings->visibleDaysCount();
}
SchedulePainterParameters::~SchedulePainterParameters() {
}

int SchedulePainterParameters::getWidth() const {
	return mWidth;
}
int SchedulePainterParameters::getHeight() const {
	return mHeight;
}

int SchedulePainterParameters::getScaleFactor() const {
	return mScaleFactor;
}

int SchedulePainterParameters::getOffsetFrameX() const {
	return mOffsetFrameX;
}
int SchedulePainterParameters::getOffsetFrameY() const {
	return mOffsetFrameY;
}

int SchedulePainterParameters::getBoardXPos() const {
	return mBoardXPos;
}
int SchedulePainterParameters::getBoardYPos() const {
	return mBoardYPos;
}
int SchedulePainterParameters::getBoardWidth() const {
	return mBoardWidth;
}
int SchedulePainterParameters::getBoardHeight() const {
	return mBoardHeight;
}

int SchedulePainterParameters::getHourCount() const {
	return mHourCount;
}
int SchedulePainterParameters::getHourHeight() const {
	return mHourHeight;
}
int SchedulePainterParameters::getDayWidth() const {
	return mDayWidth;
}

}
}
