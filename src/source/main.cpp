/* Copyright by: P.J. Grochowski */

#include "SPSchedule.h"

// Main implementation:
#ifdef GDEV_DEBUG
IMPLEMENT_APP_CONSOLE( gdev::spSchedule::SPSchedule )
#else
IMPLEMENT_APP( gdev::spSchedule::SPSchedule )
#endif
