/* Copyright by: P.J. Grochowski */

#include "DialogResolution.h"

#include "gdev/guiWX/LangSwitcher.h"
#include "gdev/utility/Strings.h"
#include "gdev/utility/Resources.h"

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/statline.h>

#include <Poco/Format.h>

namespace gdev {
namespace spSchedule {

DialogResolution::DialogResolution( wxWindow* parent ):
		wxDialog(
				parent,
				wxID_ANY,
				wxEmptyString,
				wxDefaultPosition,
				wxDefaultSize,
				wxDEFAULT_DIALOG_STYLE
		)
{
	// Set title:
	const std::string TITLE = "dialog_title_resolution";
	SetTitle( _getStrWX( TITLE ) );

	// Set icon:
	{
		wxIcon icon;
		icon.CopyFromBitmap( _getBitmapWX( "appicon.png" ) );
		SetIcon( icon );
	}

	// Set dialog resizing hints:
	SetSizeHints( wxDefaultSize, wxDefaultSize );

	// Set background window color:
	SetForegroundColour( wxColour( 0, 0, 0 ) );
	SetBackgroundColour( wxColour( 255, 255, 255 ) );

	// Create sizes:
	mSizes.push_back( wxSize( 1280, 720 ) ); // 	HD
	mSizes.push_back( wxSize( 1366, 768 ) );
	mSizes.push_back( wxSize( 1600, 900 ) );
	mSizes.push_back( wxSize( 1920, 1080 ) ); // 	FullHD
	mSizes.push_back( wxSize( 2048, 1152 ) );
	mSizes.push_back( wxSize( 2560, 1440 ) ); // 	QHD
	mSizes.push_back( wxSize( 2880, 1620 ) );
	mSizes.push_back( wxSize( 3200, 1800 ) );
	mSizes.push_back( wxSize( 3840, 2160 ) ); // 	4K

	// Create sizers:
	wxBoxSizer* sizer = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer* sizerCtrl = new wxBoxSizer( wxHORIZONTAL );

	const std::string DIALOG_RESOLUTION_LABEL = "dialog_resolution_label";
	wxStaticText* ctrlLabel = new wxStaticText( this, wxID_ANY, _getStrWX( DIALOG_RESOLUTION_LABEL ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlLabel->Wrap( -1 );
	sizerCtrl->Add( ctrlLabel, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString choices;
	for( std::size_t i=0; i<mSizes.size(); i++ ) {
		choices.Add( _fixStrWX( Poco::format( "%?dx%?d", mSizes.at(i).GetWidth(), mSizes.at(i).GetHeight() ) ) );
	}
	mCtrlChoice = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, choices );
	mCtrlChoice->SetSelection( 0 );
	sizerCtrl->Add( mCtrlChoice, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	sizer->Add( sizerCtrl, 1, wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	wxStaticLine* line = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	sizer->Add( line, 0, wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 20 );

	wxStdDialogButtonSizer* sizerBtn = new wxStdDialogButtonSizer();
	mCtrlBtnOK = new wxButton( this, wxID_OK );
	sizerBtn->AddButton( mCtrlBtnOK );
	mCtrlBtnCancel = new wxButton( this, wxID_CANCEL );
	sizerBtn->AddButton( mCtrlBtnCancel );
	sizerBtn->Realize();

	sizer->Add( sizerBtn, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 10 );

	sizer->Add( 0, 0, 0, wxBOTTOM, 5 );

	SetSizer( sizer );
	sizer->Fit( this );

	// Language switcher:
	gdev::guiWx::LangSwitcher::Ptr langSwitcher = gdev::guiWx::LangSwitcher::getInstance();
	langSwitcher->addWindow( this, TITLE );
	langSwitcher->addStaticText( ctrlLabel, DIALOG_RESOLUTION_LABEL );

	// Bind events:
	mCtrlBtnCancel->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogResolution::onBtnCancel, this );
	mCtrlBtnOK->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogResolution::onBtnOK, this );
}
DialogResolution::~DialogResolution() {
	mCtrlBtnCancel->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogResolution::onBtnCancel, this );
	mCtrlBtnOK->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogResolution::onBtnOK, this );
}

bool DialogResolution::show( wxSize& size ) {
	mCtrlChoice->SetFocus();

	Layout();
	Fit();
	Centre( wxBOTH );

	if( !ShowModal() ) {
		return false;
	}
	size = mSizes.at(mCtrlChoice->GetSelection());
	mCtrlChoice->SetSelection( 0 );
	return true;
}

void DialogResolution::onBtnCancel( wxCommandEvent& ) {
	EndModal( false );
}
void DialogResolution::onBtnOK( wxCommandEvent& ) {
	EndModal( true );
}

}
}
