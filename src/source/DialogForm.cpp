/* Copyright by: P.J. Grochowski */

#include "DialogForm.h"

#include "gdev/guiWX/CustomRefreshTrigger.h"
#include "gdev/guiWX/LangSwitcher.h"
#include "gdev/utility/Strings.h"
#include "gdev/utility/Resources.h"

#include <wx/msgdlg.h>

#include <Poco/Format.h>

namespace gdev {
namespace spSchedule {

DialogForm::DialogForm( wxWindow* parent ):
		wxDialog(
			parent,
			wxID_ANY,
			wxEmptyString,
			wxDefaultPosition,
			wxDefaultSize,
			wxDEFAULT_DIALOG_STYLE
		)
{
	// Set title:
	const std::string TITLE = "dialog_title_form";
	SetTitle( _getStrWX( TITLE ) );

	// Set icon:
	{
		wxIcon icon;
		icon.CopyFromBitmap( _getBitmapWX( "appicon.png" ) );
		SetIcon( icon );
	}

	// Set dialog resizing hints:
	SetSizeHints( wxDefaultSize, wxDefaultSize );

	// Set background window color:
	SetForegroundColour( wxColour( 0, 0, 0 ) );
	SetBackgroundColour( wxColour( 255, 255, 255 ) );

	//----------------------------------------------------------------------//

	mSettings = Settings::getInstance();

	//----------------------------------------------------------------------//

	// Create 'details' static box:
	const std::string DIALOG_FORM_DETAILS = "dialog_form_details";
	wxStaticBoxSizer* staticBoxDetails = new wxStaticBoxSizer(
			new wxStaticBox( this, wxID_ANY, _getStrWX( DIALOG_FORM_DETAILS ) ),
			wxVERTICAL
	);

	wxPanel* panelDetails = new wxPanel( staticBoxDetails->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	panelDetails->SetForegroundColour( wxColour( 0, 0, 0 ) );
	panelDetails->SetBackgroundColour( wxColour( 255, 255, 255 ) );

	// Create 'details' sizer:
	wxFlexGridSizer* layoutDetails = new wxFlexGridSizer( 0, 2, 0, 0 );
	layoutDetails->AddGrowableCol( 1 );
	layoutDetails->SetFlexibleDirection( wxBOTH );
	layoutDetails->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	const std::string DIALOG_FORM_SUBJECT = "dialog_form_subject";
	wxStaticText* ctrlSTSubject = new wxStaticText( panelDetails, wxID_ANY, _getStrWX( DIALOG_FORM_SUBJECT ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTSubject->Wrap( -1 );
	layoutDetails->Add( ctrlSTSubject, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	mCtrlEditSubject = new wxTextCtrl( panelDetails, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	mCtrlEditSubject->SetMaxLength( 100 );
	layoutDetails->Add( mCtrlEditSubject, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	const std::string DIALOG_FORM_LECTURER = "dialog_form_lecturer";
	wxStaticText* ctrlSTLecturer = new wxStaticText( panelDetails, wxID_ANY, _getStrWX( DIALOG_FORM_LECTURER ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTLecturer->Wrap( -1 );
	layoutDetails->Add( ctrlSTLecturer, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	mCtrlEditTutor = new wxTextCtrl( panelDetails, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	mCtrlEditTutor->SetMaxLength( 100 );
	layoutDetails->Add( mCtrlEditTutor, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	const std::string DIALOG_FORM_PLACE = "dialog_form_place";
	wxStaticText* ctrlSTPlace = new wxStaticText( panelDetails, wxID_ANY, _getStrWX( DIALOG_FORM_PLACE ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTPlace->Wrap( -1 );
	layoutDetails->Add( ctrlSTPlace, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	mCtrlEditPlace = new wxTextCtrl( panelDetails, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	mCtrlEditPlace->SetMaxLength( 100 );
	layoutDetails->Add( mCtrlEditPlace, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	const std::string DIALOG_FORM_TYPE = "dialog_form_type";
	wxStaticText* ctrlSTType = new wxStaticText( panelDetails, wxID_ANY, _getStrWX( DIALOG_FORM_TYPE ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTType->Wrap( -1 );
	layoutDetails->Add( ctrlSTType, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	std::vector < std::string > labelsType = mSettings->getCourseTypeLabelsVector();
	wxArrayString choicesType;
	for( std::size_t i=0; i<labelsType.size(); i++ ) {
		choicesType.Add( _getStrWX( labelsType.at( i ) ) );
	}
	mCtrlCBType = new wxChoice( panelDetails, wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesType );
	mCtrlCBType->SetSelection( 0 );
	layoutDetails->Add( mCtrlCBType, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	panelDetails->SetSizer( layoutDetails );
	panelDetails->Layout();
	layoutDetails->Fit( panelDetails );
	staticBoxDetails->Add( panelDetails, 1, wxEXPAND, 5 );

	// Insert 'details' sizer in 'details' static box:
	//staticBoxDetails->Add( layoutDetails, 1, wxEXPAND|wxALL, 5 );

	//----------------------------------------------------------------------//

	// Create 'date' static box:
	const std::string DIALOG_FORM_DATE = "dialog_form_date";
	wxStaticBoxSizer* staticBoxDate = new wxStaticBoxSizer(
			new wxStaticBox( this, wxID_ANY, _getStrWX( DIALOG_FORM_DATE ) ),
			wxVERTICAL
	);

	wxPanel* panelDate = new wxPanel( staticBoxDate->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	panelDate->SetForegroundColour( wxColour( 0, 0, 0 ) );
	panelDate->SetBackgroundColour( wxColour( 255, 255, 255 ) );

	// Create 'date' sizer:
	wxFlexGridSizer* layoutDate = new wxFlexGridSizer( 0, 2, 0, 0 );
	layoutDate->SetFlexibleDirection( wxBOTH );
	layoutDate->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	const std::string DIALOG_FORM_DAY = "dialog_form_day";
	wxStaticText* ctrlSTDay = new wxStaticText( panelDate, wxID_ANY, _getStrWX( DIALOG_FORM_DAY ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTDay->Wrap( -1 );
	layoutDate->Add( ctrlSTDay, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	std::vector < std::string > labelsDay = mSettings->getDaysLabelsVector();
	wxArrayString choicesDay;
	for( std::size_t i=0; i<labelsDay.size(); i++ ) {
		choicesDay.Add( _getStrWX( labelsDay.at( i ) ) );
	}
	mCtrlCBDay = new wxChoice( panelDate, wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesDay );
	mCtrlCBDay->SetSelection( 0 );
	layoutDate->Add( mCtrlCBDay, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	const std::string DIALOG_FORM_WEEKCYCLE = "dialog_form_weekcycle";
	wxStaticText* ctrlSTWeekCycle = new wxStaticText( panelDate, wxID_ANY, _getStrWX( DIALOG_FORM_WEEKCYCLE ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTWeekCycle->Wrap( -1 );
	layoutDate->Add( ctrlSTWeekCycle, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	std::vector < std::string > labelsWeekCycle = mSettings->getWeekCycleLabelsVector();
	wxArrayString choicesWeekCycle;
	for( std::size_t i=0; i<labelsWeekCycle.size(); i++ ) {
		choicesWeekCycle.Add( _getStrWX( labelsWeekCycle.at( i ) ) );
	}
	mCtrlCBWeekCycle = new wxChoice( panelDate, wxID_ANY, wxDefaultPosition, wxDefaultSize, choicesWeekCycle );
	mCtrlCBWeekCycle->SetSelection( 0 );
	layoutDate->Add( mCtrlCBWeekCycle, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	// Create 'start' and 'end' layouts:
	wxBoxSizer* layoutStart = new wxBoxSizer( wxHORIZONTAL );
	wxBoxSizer* layoutEnd = new wxBoxSizer( wxHORIZONTAL );

	mCtrlSpinBeginH = new wxSpinCtrl( panelDate, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS , 0, 23 );
	layoutStart->Add( mCtrlSpinBeginH, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	mCtrlSpinBeginM = new wxSpinCtrl( panelDate, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 59 );
	layoutStart->Add( mCtrlSpinBeginM, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	mCtrlSpinEndH = new wxSpinCtrl( panelDate, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 23 );
	layoutEnd->Add( mCtrlSpinEndH, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	mCtrlSpinEndM = new wxSpinCtrl( panelDate, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 59 );
	layoutEnd->Add( mCtrlSpinEndM, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	// Create 'start' 'date' label:
	const std::string DIALOG_FORM_BEGIN = "dialog_form_begin";
	wxStaticText* ctrlSTBegin = new wxStaticText( panelDate, wxID_ANY, _getStrWX( DIALOG_FORM_BEGIN ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTBegin->Wrap( -1 );
	layoutDate->Add( ctrlSTBegin, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	// Add 'start' layout to 'date' layout:
	layoutDate->Add( layoutStart, 1, wxEXPAND, 5 );

	// Create 'end' 'date' label:
	const std::string DIALOG_FORM_END = "dialog_form_end";
	wxStaticText* ctrlSTEnd = new wxStaticText( panelDate, wxID_ANY, _getStrWX( DIALOG_FORM_END ), wxDefaultPosition, wxDefaultSize, 0 );
	ctrlSTEnd->Wrap( -1 );
	layoutDate->Add( ctrlSTEnd, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	// Add 'end' layout to 'date' layout:
	layoutDate->Add( layoutEnd, 1, wxEXPAND, 5 );

	panelDate->SetSizer( layoutDate );
	panelDate->Layout();
	layoutDate->Fit( panelDate );
	staticBoxDate->Add( panelDate, 1, wxEXPAND, 5 );

	// Insert 'date' sizer in 'date' static box:
	//staticBoxDate->Add( layoutDate, 1, wxEXPAND, 5 );

	//----------------------------------------------------------------------//

	// Create standard dialog buttons:
	wxStdDialogButtonSizer* layoutBtn = new wxStdDialogButtonSizer();
	mBtnOK = new wxButton( this, wxID_OK );
	layoutBtn->AddButton( mBtnOK );
	mBtnCancel = new wxButton( this, wxID_CANCEL );
	layoutBtn->AddButton( mBtnCancel );
	layoutBtn->Realize();

	// Create main horizontal sizer:
	wxBoxSizer* layoutMainH = new wxBoxSizer( wxHORIZONTAL );
	layoutMainH->Add( staticBoxDetails, 1, wxEXPAND|wxALL, 5 );
	layoutMainH->Add( staticBoxDate, 1, wxEXPAND|wxALL, 5 );

	// Create main vertical sizer:
	wxBoxSizer* layoutMainV = new wxBoxSizer( wxVERTICAL );
	layoutMainV->Add( layoutMainH, 1, wxEXPAND, 5 );
	layoutMainV->Add( layoutBtn, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

	// Set main window sizer:
	SetSizer( layoutMainV );
	layoutMainV->Fit( this );

	// Bind events:
	mBtnCancel->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogForm::onBtnCancel, this );
	mBtnOK->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogForm::onBtnOK, this );

	// Language switcher:
	gdev::guiWx::LangSwitcher::Ptr langSwitcher = gdev::guiWx::LangSwitcher::getInstance();
	langSwitcher->addWindow( this, TITLE );
	langSwitcher->addCustomRefreshTrigger(
			new gdev::guiWx::CustomRefreshTrigger < DialogForm >( *this, &DialogForm::refresh )
	);
	langSwitcher->addStaticBox( staticBoxDetails->GetStaticBox(), DIALOG_FORM_DETAILS );
	langSwitcher->addStaticBox( staticBoxDate->GetStaticBox(), DIALOG_FORM_DATE );
	langSwitcher->addStaticText( ctrlSTSubject, DIALOG_FORM_SUBJECT );
	langSwitcher->addStaticText( ctrlSTLecturer, DIALOG_FORM_LECTURER );
	langSwitcher->addStaticText( ctrlSTPlace, DIALOG_FORM_PLACE );
	langSwitcher->addStaticText( ctrlSTType, DIALOG_FORM_TYPE );
	langSwitcher->addStaticText( ctrlSTDay, DIALOG_FORM_DAY );
	langSwitcher->addStaticText( ctrlSTWeekCycle, DIALOG_FORM_WEEKCYCLE );
	langSwitcher->addStaticText( ctrlSTBegin, DIALOG_FORM_BEGIN );
	langSwitcher->addStaticText( ctrlSTEnd, DIALOG_FORM_END );
	langSwitcher->addComboBox( mCtrlCBType, labelsType );
	langSwitcher->addComboBox( mCtrlCBDay, labelsDay );
	langSwitcher->addComboBox( mCtrlCBWeekCycle, labelsWeekCycle );
}
DialogForm::~DialogForm() {
	mBtnCancel->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogForm::onBtnCancel, this );
	mBtnOK->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogForm::onBtnOK, this );
}

void DialogForm::onBtnCancel( wxCommandEvent& ) {
	EndModal( false );
}
void DialogForm::onBtnOK( wxCommandEvent& )
{
	std::string subject = _fixStrSTL( mCtrlEditSubject->GetValue() );
	std::string tutor 	= _fixStrSTL( mCtrlEditTutor->GetValue() );
	std::string place 	= _fixStrSTL( mCtrlEditPlace->GetValue() );
	Settings::CourseType courseType = (Settings::CourseType)mCtrlCBType->GetSelection();
	Settings::DayOfWeek day = (Settings::DayOfWeek)mCtrlCBDay->GetSelection();
	Settings::WeekCycle weekCycle = (Settings::WeekCycle)mCtrlCBWeekCycle->GetSelection();
	int startHour 	= mCtrlSpinBeginH->GetValue();
	int startMinute = mCtrlSpinBeginM->GetValue();
	int endHour 	= mCtrlSpinEndH->GetValue();
	int endMinute 	= mCtrlSpinEndM->GetValue();

	std::pair < int, int > range = mSettings->getRangeHours();
	range = std::pair < int, int >( range.first*60, range.second*60 );
	int start = ( startHour * 60 ) + startMinute;
	int end = ( endHour * 60 ) + endMinute;
	int minDuration = mSettings->getMinDuration();
	if( start > end )
	{
		wxMessageBox(
				_getStrWX( "msgbox_error_corse_time_bounds_swaped" ),
				_getStrWX( "error" ),
				wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
		);
		return;
	}
	if( ( end - start ) < minDuration )
	{
		std::string msg = Poco::format(
				gdev::utility::Strings::getInstance()->getString( "msgbox_error_course_duration" ).c_str(),
				minDuration
		);
		wxMessageBox(
				_fixStrWX( msg ),
				_getStrWX( "error" ),
				wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
		);
		return;
	}
	if( start < range.first || end > range.second )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_warning_course_invisible_time" ),
				_getStrWX( "warning" ),
				wxYES_NO|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}

	if( !mSettings->isDayVisible( day ) )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_warning_course_invisible_day" ),
				_getStrWX( "warning" ),
				wxYES_NO|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}

	if( mWrapperCourse->mCourse.isNull() )
	{
		mWrapperCourse->mCourse = new Course(
				subject,
				tutor,
				place,
				courseType,
				day,
				weekCycle,
				startHour,
				startMinute,
				endHour,
				endMinute
		);

		EndModal( true );
		return;
	}

	Poco::Tuple < int, int, int, int > timeOriginal = mWrapperCourse->mCourse->getTime();
	if(
			( mWrapperCourse->mCourse->getSubject().compare( subject ) ) ||
			( mWrapperCourse->mCourse->getTutor().compare( tutor ) ) ||
			( mWrapperCourse->mCourse->getPlace().compare( place ) ) ||
			( mWrapperCourse->mCourse->getType() != courseType ) ||
			( mWrapperCourse->mCourse->getDay() != day ) ||
			( mWrapperCourse->mCourse->getWeekCycle() != weekCycle ) ||
			( timeOriginal.get<0>() != startHour ) ||
			( timeOriginal.get<1>() != startMinute ) ||
			( timeOriginal.get<2>() != endHour ) ||
			( timeOriginal.get<3>() != endMinute )
	)
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_question_modify_course" ),
				_getStrWX( "question" ),
				wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}
	}
	mWrapperCourse->mCourse->setSubject( subject );
	mWrapperCourse->mCourse->setTutor( tutor );
	mWrapperCourse->mCourse->setPlace( place );
	mWrapperCourse->mCourse->setType( courseType );
	mWrapperCourse->mCourse->setDay( day );
	mWrapperCourse->mCourse->setWeekCycle( weekCycle );
	mWrapperCourse->mCourse->setTime(
			startHour,
			startMinute,
			endHour,
			endMinute
	);

	EndModal( true );
}

bool DialogForm::show( Course::Ptr& course )
{
	bool state = false;
	populate( course );
	refresh();
	state = ShowModal();
	clean();
	return state;
}

void DialogForm::refresh()
{
	Layout();
	Fit();
	Centre( wxBOTH );
}

void DialogForm::populate( Course::Ptr& course )
{
	mWrapperCourse = new WrapperCourse( course );
	if( course.isNull() ) {
		clean();
		return;
	}

	std::string subject = mWrapperCourse->mCourse->getSubject();
	std::string tutor = mWrapperCourse->mCourse->getTutor();
	std::string place = mWrapperCourse->mCourse->getPlace();
	mCtrlEditSubject->SetValue( _fixStrWX( subject ) );
	mCtrlEditTutor->SetValue( _fixStrWX( tutor ) );
	mCtrlEditPlace->SetValue( _fixStrWX( place ) );

	mCtrlCBType->SetSelection( (int)mWrapperCourse->mCourse->getType() );
	mCtrlCBDay->SetSelection( (int)mWrapperCourse->mCourse->getDay() );
	mCtrlCBWeekCycle->SetSelection( (int)mWrapperCourse->mCourse->getWeekCycle() );

	Poco::Tuple < int, int, int, int > time = mWrapperCourse->mCourse->getTime();
	mCtrlSpinBeginH->SetValue( time.get<0>() );
	mCtrlSpinBeginM->SetValue( time.get<1>() );
	mCtrlSpinEndH->SetValue( time.get<2>() );
	mCtrlSpinEndM->SetValue( time.get<3>() );
}
void DialogForm::clean()
{
	mCtrlEditSubject->SetValue( wxEmptyString );
	mCtrlEditTutor->SetValue( wxEmptyString );
	mCtrlEditPlace->SetValue( wxEmptyString );

	mCtrlCBType->SetSelection( 0 );
	mCtrlCBDay->SetSelection( 0 );
	mCtrlCBWeekCycle->SetSelection( 0 );

	mCtrlSpinBeginH->SetValue( 0 );
	mCtrlSpinEndH->SetValue( 0 );
	mCtrlSpinBeginM->SetValue( 0 );
	mCtrlSpinEndM->SetValue( 0 );

	mCtrlEditSubject->SetFocus();
}

}
}
