/* Copyright by: P.J. Grochowski */

#include "DialogSettings.h"

#include "gdev/guiWX/CustomRefreshTrigger.h"
#include "gdev/guiWX/LangSwitcher.h"
#include "gdev/utility/Strings.h"
#include "gdev/utility/Resources.h"

#include <wx/stattext.h>
#include <wx/statline.h>
#include <wx/msgdlg.h>

namespace gdev {
namespace spSchedule {

DialogSettings::DialogSettings( wxWindow* parent ):
		wxDialog( parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	// Set title:
	const std::string TITLE = "dialog_title_settings";
	SetTitle( _getStrWX( TITLE ) );

	// Set icon:
	{
		wxIcon icon;
		icon.CopyFromBitmap( _getBitmapWX( "appicon.png" ) );
		SetIcon( icon );
	}

	// Set dialog resizing hints:
	SetSizeHints( wxDefaultSize, wxDefaultSize );

	// Set background window color:
	SetForegroundColour( wxColour( 0, 0, 0 ) );
	SetBackgroundColour( wxColour( 255, 255, 255 ) );

	//----------------------------------------------------------------------//

	mSettings = Settings::getInstance();

	//----------------------------------------------------------------------//

	wxBoxSizer* sizer = new wxBoxSizer( wxVERTICAL );

	wxFlexGridSizer* sizerCtrl = new wxFlexGridSizer( 1, 2, 0, 0 );
	sizerCtrl->AddGrowableCol( 0 );
	sizerCtrl->AddGrowableCol( 1 );
	sizerCtrl->SetFlexibleDirection( wxBOTH );
	sizerCtrl->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	const std::string DIALOG_SETTINGS_APPEARANCE = "dialog_settings_appearance";
	wxStaticBoxSizer* sizerBoxAppearance = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _getStrWX( DIALOG_SETTINGS_APPEARANCE ) ), wxVERTICAL );

	wxPanel* panelAppearance = new wxPanel( sizerBoxAppearance->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	panelAppearance->SetForegroundColour( wxColour( 0, 0, 0 ) );
	panelAppearance->SetBackgroundColour( wxColour( 255, 255, 255 ) );

	wxBoxSizer* sizerPanelAppearance = new wxBoxSizer( wxVERTICAL );

	wxFlexGridSizer* sizerEdges = new wxFlexGridSizer( 0, 2, 0, 0 );
	sizerEdges->AddGrowableCol( 0 );
	sizerEdges->AddGrowableCol( 1 );
	sizerEdges->SetFlexibleDirection( wxBOTH );
	sizerEdges->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	const std::string DIALOG_SETTINGS_EDGES_RADIUS = "dialog_settings_edges_radius";
	wxStaticText* lblEdges = new wxStaticText( panelAppearance, wxID_ANY, _getStrWX( DIALOG_SETTINGS_EDGES_RADIUS ), wxDefaultPosition, wxDefaultSize, 0 );
	lblEdges->Wrap( -1 );
	sizerEdges->Add( lblEdges, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	std::pair < int, int > edgesRadiusBounds = mSettings->getBoundsEdgesRadius();
	mSpinEdges = new wxSpinCtrl( panelAppearance, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, edgesRadiusBounds.first, edgesRadiusBounds.second, 0 );
	sizerEdges->Add( mSpinEdges, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	sizerPanelAppearance->Add( sizerEdges, 0, wxEXPAND, 5 );

	//----------------------------------------------------------------------//

	wxStaticLine* ctrlStaticLine = new wxStaticLine( panelAppearance, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	sizerPanelAppearance->Add( ctrlStaticLine, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	//----------------------------------------------------------------------//

	wxFlexGridSizer* sizerColors = new wxFlexGridSizer( 0, 2, 0, 0 );
	sizerColors->AddGrowableCol( 0 );
	sizerColors->AddGrowableCol( 1 );
	sizerColors->SetFlexibleDirection( wxBOTH );
	sizerColors->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	std::map < Settings::CourseType, std::string > courseTypeLbls = mSettings->getCourseTypeLabels();
	std::vector < std::pair < wxStaticText*, std::string > > courseTypeCtrls;
	{
		typedef std::map < Settings::CourseType, std::string >::iterator It;
		for( It it=courseTypeLbls.begin(), itEnd=courseTypeLbls.end(); it!=itEnd; it++ )
		{
			wxStaticText* courseLbl = new wxStaticText( panelAppearance, wxID_ANY, _getStrWX( it->second ), wxDefaultPosition, wxDefaultSize, 0 );
			courseLbl->Wrap( -1 );
			sizerColors->Add( courseLbl, 0, wxEXPAND|wxALIGN_CENTER_VERTICAL|wxTOP|wxRIGHT|wxLEFT, 5 );

			wxColourPickerCtrl* courseCPickerCtrl = new wxColourPickerCtrl( panelAppearance, wxID_ANY, wxColour( 0, 0, 0 ), wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
			sizerColors->Add( courseCPickerCtrl, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );

			courseTypeCtrls.push_back( std::pair < wxStaticText*, std::string >( courseLbl, it->second ) );

			mCPickers[it->first] = courseCPickerCtrl;
		}
	}

	sizerPanelAppearance->Add( sizerColors, 1, wxEXPAND, 5 );

	panelAppearance->SetSizer( sizerPanelAppearance );
	panelAppearance->Layout();
	sizerPanelAppearance->Fit( panelAppearance );
	sizerBoxAppearance->Add( panelAppearance, 1, wxEXPAND, 5 );

	sizerCtrl->Add( sizerBoxAppearance, 1, wxEXPAND|wxALL, 5 );

	//----------------------------------------------------------------------//

	const std::string DIALOG_SETTINGS_VISIBILITY = "dialog_settings_visibility";
	wxStaticBoxSizer* sizerBoxVisibility = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _getStrWX( DIALOG_SETTINGS_VISIBILITY ) ), wxVERTICAL );

	wxPanel* panelVisibility = new wxPanel( sizerBoxVisibility->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	panelVisibility->SetForegroundColour( wxColour( 0, 0, 0 ) );
	panelVisibility->SetBackgroundColour( wxColour( 255, 255, 255 ) );

	wxBoxSizer* sizerPanelVisibility;
	sizerPanelVisibility = new wxBoxSizer( wxVERTICAL );

	wxFlexGridSizer* sizerVisibility = new wxFlexGridSizer( 0, 2, 0, 0 );
	sizerVisibility->AddGrowableCol( 0 );
	sizerVisibility->AddGrowableCol( 1 );
	sizerVisibility->SetFlexibleDirection( wxBOTH );
	sizerVisibility->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	const std::string DIALOG_SETTINGS_HOUR_BOUND_UPPER = "dialog_settings_hour_bound_upper";
	wxStaticText* lblHourUpper = new wxStaticText( panelVisibility, wxID_ANY, _getStrWX( DIALOG_SETTINGS_HOUR_BOUND_UPPER ), wxDefaultPosition, wxDefaultSize, 0 );
	lblHourUpper->Wrap( -1 );
	sizerVisibility->Add( lblHourUpper, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );

	mSpinHourUpper = new wxSpinCtrl( panelVisibility, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 24, 0 );
	sizerVisibility->Add( mSpinHourUpper, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );

	const std::string DIALOG_SETTINGS_HOUR_BOUND_LOWER = "dialog_settings_hour_bound_lower";
	wxStaticText* lblHourLower = new wxStaticText( panelVisibility, wxID_ANY, _getStrWX( DIALOG_SETTINGS_HOUR_BOUND_LOWER ), wxDefaultPosition, wxDefaultSize, 0 );
	lblHourLower->Wrap( -1 );
	sizerVisibility->Add( lblHourLower, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	mSpinHourLower = new wxSpinCtrl( panelVisibility, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 24, 0 );
	sizerVisibility->Add( mSpinHourLower, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	sizerPanelVisibility->Add( sizerVisibility, 0, wxEXPAND, 5 );

	//----------------------------------------------------------------------//

	wxPanel* panelDay = new wxPanel( panelVisibility, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSIMPLE_BORDER|wxTAB_TRAVERSAL );
	panelDay->SetForegroundColour( wxColour( 0, 0, 0 ) );
	panelDay->SetBackgroundColour( wxColour( 255, 255, 255 ) );
	wxBoxSizer* sizerDay = new wxBoxSizer( wxVERTICAL );

	{
		std::map < Settings::DayOfWeek, std::string > dayLabels = mSettings->getDaysLabels();
		typedef std::map < Settings::DayOfWeek, std::string >::iterator It;
		for( It it=dayLabels.begin(), itEnd=dayLabels.end(); it!=itEnd; it++ ) {
			wxCheckBox* checkCtrl = new wxCheckBox( panelDay, wxID_ANY, _getStrWX( it->second ), wxDefaultPosition, wxDefaultSize, 0 );
			sizerDay->Add( checkCtrl, 0, wxTOP|wxRIGHT|wxLEFT|wxEXPAND, 5 );
			mDCheck[it->first] = checkCtrl;
		}
	}

	sizerDay->Add( 0, 5, 1, wxEXPAND, 5 );

	panelDay->SetSizer( sizerDay );
	panelDay->Layout();
	sizerDay->Fit( panelDay );
	sizerPanelVisibility->Add( panelDay, 1, wxEXPAND|wxALL, 5 );

	panelVisibility->SetSizer( sizerPanelVisibility );
	panelVisibility->Layout();
	sizerPanelVisibility->Fit( panelVisibility );
	sizerBoxVisibility->Add( panelVisibility, 1, wxEXPAND, 5 );

	sizerCtrl->Add( sizerBoxVisibility, 1, wxEXPAND|wxALL, 5 );

	sizer->Add( sizerCtrl, 1, wxEXPAND, 5 );

	wxBoxSizer* sizerBtnCtrl;
	sizerBtnCtrl = new wxBoxSizer( wxHORIZONTAL );

	wxStdDialogButtonSizer* sizerBtn = new wxStdDialogButtonSizer();
	mBtnOK = new wxButton( this, wxID_OK );
	sizerBtn->AddButton( mBtnOK );
	mBtnCancel = new wxButton( this, wxID_CANCEL );
	sizerBtn->AddButton( mBtnCancel );
	sizerBtn->Realize();

	sizerBtnCtrl->Add( sizerBtn, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxLEFT, 5 );

	const std::string DIALOG_SETTINGS_RESET = "dialog_settings_reset";
	mBtnReset = new wxButton( this, wxID_ANY, _getStrWX( DIALOG_SETTINGS_RESET ), wxDefaultPosition, wxDefaultSize, 0 );
	sizerBtnCtrl->Add( mBtnReset, 0, wxALL, 5 );

	sizer->Add( sizerBtnCtrl, 0, wxALIGN_CENTER_HORIZONTAL, 5 );

	SetSizer( sizer );
	Layout();
	Fit();
	sizer->Fit( this );

	Centre( wxBOTH );

	// Bind events:
	mBtnCancel->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogSettings::onBtnCancel, this );
	mBtnOK->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogSettings::onBtnOK, this );
	mBtnReset->Bind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogSettings::onBtnReset, this );

	// Language switcher:
	gdev::guiWx::LangSwitcher::Ptr langSwitcher = gdev::guiWx::LangSwitcher::getInstance();
	langSwitcher->addWindow( this, TITLE );
	langSwitcher->addCustomRefreshTrigger(
			new gdev::guiWx::CustomRefreshTrigger < DialogSettings >( *this, &DialogSettings::refresh )
	);
	{
		typedef std::vector < std::pair < wxStaticText*, std::string > >::iterator It;
		for( It it=courseTypeCtrls.begin(), itEnd=courseTypeCtrls.end(); it!=itEnd; it++ ) {
			langSwitcher->addStaticText( it->first, it->second );
		}
	}
	{
		std::map < Settings::DayOfWeek, std::string > dayLabels = mSettings->getDaysLabels();
		typedef std::map < Settings::DayOfWeek, wxCheckBox* >::iterator It;
		for( It it=mDCheck.begin(), itEnd=mDCheck.end(); it!=itEnd; it++ ) {
			langSwitcher->addCheckBox( it->second, dayLabels[it->first] );
		}
	}
	langSwitcher->addStaticBox( sizerBoxAppearance->GetStaticBox(), DIALOG_SETTINGS_APPEARANCE );
	langSwitcher->addStaticBox( sizerBoxVisibility->GetStaticBox(), DIALOG_SETTINGS_VISIBILITY );
	langSwitcher->addStaticText( lblEdges, DIALOG_SETTINGS_EDGES_RADIUS );
	langSwitcher->addStaticText( lblHourUpper, DIALOG_SETTINGS_HOUR_BOUND_UPPER );
	langSwitcher->addStaticText( lblHourLower, DIALOG_SETTINGS_HOUR_BOUND_LOWER );
	langSwitcher->addButton( mBtnReset, DIALOG_SETTINGS_RESET );
}
DialogSettings::~DialogSettings() {
	mBtnCancel->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogSettings::onBtnCancel, this );
	mBtnOK->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogSettings::onBtnOK, this );
	mBtnReset->Unbind( wxEVT_COMMAND_BUTTON_CLICKED, &DialogSettings::onBtnReset, this );
}

void DialogSettings::onBtnCancel( wxCommandEvent& ) {
	EndModal( false );
}
void DialogSettings::onBtnOK( wxCommandEvent& ) {
	bool state = false;

	int edgesRadius = mSpinEdges->GetValue();
	if( edgesRadius != mSettings->getEdgesRadius() ) {
		state = true;
	}

	int hourBoundUpper = mSpinHourUpper->GetValue();
	int hourBoundLower = mSpinHourLower->GetValue();
	{
		std::pair < int, int > hourRange = mSettings->getRangeHours();
		if( hourBoundUpper < hourBoundLower )
		{
			wxMessageBox(
					_getStrWX( "msgbox_error_hour_range_swaped" ),
					_getStrWX( "error" ),
					wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
			);
			return;
		}
		if( hourBoundUpper == hourBoundLower )
		{
			wxMessageBox(
					_getStrWX( "msgbox_error_hour_range_none" ),
					_getStrWX( "error" ),
					wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
			);
			return;
		}
		if( ( hourBoundUpper != hourRange.second ) || ( hourBoundLower != hourRange.first ) ) {
			state = true;
		}
	}

	{
		bool flagIsOneVisible = false;
		std::map < Settings::DayOfWeek, bool > dayVisibility = mSettings->getDaysVisibility();
		typedef std::map < Settings::DayOfWeek, wxCheckBox* >::iterator It;
		for( It it=mDCheck.begin(), itEnd=mDCheck.end(); it!=itEnd; it++ )
		{
			const bool checkState = it->second->IsChecked();
			if( dayVisibility[it->first] != checkState ) {
				state = true;
			}
			if( it->second->IsChecked() ) {
				flagIsOneVisible = true;
			}
			if( state && flagIsOneVisible ) {
				break;
			}
		}
		if( !flagIsOneVisible )
		{
			wxMessageBox(
					_getStrWX( "msgbox_error_no_visible_days" ),
					_getStrWX( "error" ),
					wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
			);
			return;
		}
	}

	{
		typedef std::map < Settings::CourseType, wxColourPickerCtrl* >::iterator It;
		for( It it=mCPickers.begin(), itEnd=mCPickers.end(); it!=itEnd; it++ ) {
			if( it->second->GetColour() != mSettings->getCourseTypeColour( it->first ) ) {
				state = true;
				break;
			}
		}
	}

	if( state )
	{
		if( wxMessageBox(
				_getStrWX( "msgbox_question_modify_settings" ),
				_getStrWX( "question" ),
				wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
			) != wxYES )
		{
			return;
		}

		mSettings->setEdgesRadius( edgesRadius );
		mSettings->setRangeHour( std::pair < int, int >( hourBoundLower, hourBoundUpper ) );
		typedef std::map < Settings::DayOfWeek, wxCheckBox* >::iterator It;
		for( It it=mDCheck.begin(), itEnd=mDCheck.end(); it!=itEnd; it++ ) {
			mSettings->setDayVisibility( it->first, it->second->GetValue() );
		}
		{
			typedef std::map < Settings::CourseType, wxColourPickerCtrl* >::iterator It;
			for( It it=mCPickers.begin(), itEnd=mCPickers.end(); it!=itEnd; it++ ) {
				mSettings->setCourseTypeColour( it->first, it->second->GetColour() );
			}
		}

		mSettings->save();
	}

	EndModal( state );
}
void DialogSettings::onBtnReset( wxCommandEvent& ) {
	if( wxMessageBox(
			_getStrWX( "msgbox_question_reset_settings" ),
			_getStrWX( "question" ),
			wxYES_NO|wxICON_QUESTION|wxSTAY_ON_TOP|wxCENTER
		) != wxYES )
	{
		return;
	}

	mSettings->reset();
	EndModal( true );
}

void DialogSettings::refresh() {
	Layout();
	Fit();
	Centre( wxBOTH );
}

bool DialogSettings::show() {
	bool state = false;
	populate();
	refresh();
	state = ShowModal();
	return state;
}

void DialogSettings::populate() {
	mSettings->load();

	mSpinEdges->SetValue( mSettings->getEdgesRadius() );
	{
		std::pair < int, int > hourRange = mSettings->getRangeHours();
		mSpinHourUpper->SetValue( hourRange.second );
		mSpinHourLower->SetValue( hourRange.first );
	}
	{
		std::map < Settings::DayOfWeek, bool > dayVisibility = mSettings->getDaysVisibility();
		typedef std::map < Settings::DayOfWeek, bool >::iterator It;
		for( It it=dayVisibility.begin(), itEnd=dayVisibility.end(); it!=itEnd; it++ ) {
			(mDCheck[it->first])->SetValue( it->second );
		}
	}
	{
		std::map < Settings::CourseType, wxColour > crsColors = mSettings->getCourseTypesColours();
		typedef std::map < Settings::CourseType, wxColour >::iterator It;
		for( It it=crsColors.begin(), itEnd=crsColors.end(); it!=itEnd; it++ ) {
			(mCPickers[it->first])->SetColour( it->second );
		}
	}
}

}
}
