/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_DIALOGRESOLUTION_H_
#define SRC_INCLUDE_DIALOGRESOLUTION_H_

#include <wx/dialog.h>
#include <wx/choice.h>
#include <wx/button.h>

#include <vector>

namespace gdev {
namespace spSchedule {

class DialogResolution:
		public wxDialog
{
	public:
		DialogResolution( wxWindow* parent );
		virtual ~DialogResolution();

		bool show( wxSize& size );

	protected:
		void onBtnCancel( wxCommandEvent& );
		void onBtnOK( wxCommandEvent& );

		std::vector < wxSize > mSizes;

		wxChoice* mCtrlChoice;

		wxButton* mCtrlBtnOK;
		wxButton* mCtrlBtnCancel;
};

}
}

#endif
