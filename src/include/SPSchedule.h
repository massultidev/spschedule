/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_SP_SCHEDULE_H_
#define SRC_INCLUDE_SP_SCHEDULE_H_

#include <wx/cmdline.h>

#include "gdev/appWx/App.h"
#include "gdev/guiWX/SplashScreen.h"
#include "gdev/utility/osguard.h"

#include "Settings.h"
#include "Frame.h"

namespace gdev {
namespace spSchedule {

class SPSchedule:
		public gdev::appWx::App
{
	public:
		SPSchedule();
		virtual ~SPSchedule();

		bool init();

		#if _IS_LINUX
			void OnInitCmdLine(wxCmdLineParser& parser);
			bool OnCmdLineParsed(wxCmdLineParser& parser);
			bool OnCmdLineHelp(wxCmdLineParser& parser);
			bool OnCmdLineError(wxCmdLineParser& parser);
		#endif

	private:
		gdev::guiWx::SplashScreen* mSplash;
		Settings::Ptr mSettings;
		Frame* mFrame;
};

}
}

#endif
