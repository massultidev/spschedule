/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_SCHEDULEPAINTER_H_
#define SRC_INCLUDE_SCHEDULEPAINTER_H_

#include "Settings.h"
#include "Course.h"
#include "SchedulePainterParameters.h"

#include <wx/dcgraph.h>

namespace gdev {
namespace spSchedule {

class SchedulePainter
{
	public:
		typedef Poco::SharedPtr < SchedulePainter > Ptr;

		SchedulePainter( wxWindow* parent );
		SchedulePainter( const wxSize& size );
		virtual ~SchedulePainter();

		void drawBoard( wxGCDC& dc ) const;
		Poco::SharedPtr < wxRect > getCourseRect( Course::Ptr course ) const;

	protected:
		SchedulePainterParameters::Ptr getParameters() const;

		Poco::SharedPtr < wxRect > getCourseRectInternal( Course::Ptr course, SchedulePainterParameters::Ptr params ) const;
		void drawThickLine( wxGCDC& dc, int x1, int y1, int x2, int y2, int size ) const;
		int quartersCount( int minutes ) const;
		wxString fitString( const wxGCDC& dc, const wxString& str, const wxRect& rect, bool ellipsize=false ) const;

		Settings::Ptr mSettings;
		Poco::SharedPtr < std::vector < Course::Ptr > > mCourses;
		wxWindow* mParent;
		wxSize mSize;
		bool mIsImagePainter;
};

}
}

#endif
