/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_DIALOGSETTINGS_H_
#define SRC_INCLUDE_DIALOGSETTINGS_H_

#include "Settings.h"

#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/spinctrl.h>
#include <wx/checkbox.h>
#include <wx/clrpicker.h>

#include <map>

namespace gdev {
namespace spSchedule {

class DialogSettings:
		public wxDialog
{
	public:
		DialogSettings( wxWindow* parent );
		virtual ~DialogSettings();

		void refresh();

		bool show();

	protected:
		void onBtnCancel( wxCommandEvent& );
		void onBtnOK( wxCommandEvent& );
		void onBtnReset( wxCommandEvent& );

		void populate();

		Settings::Ptr mSettings;

		std::map < Settings::CourseType, wxColourPickerCtrl* > mCPickers;
		std::map < Settings::DayOfWeek, wxCheckBox* > mDCheck;

		wxSpinCtrl* mSpinEdges;
		wxSpinCtrl* mSpinHourUpper;
		wxSpinCtrl* mSpinHourLower;

		wxButton* mBtnOK;
		wxButton* mBtnCancel;
		wxButton* mBtnReset;
};

}
}

#endif
