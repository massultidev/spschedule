/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_DIALOGFORM_H_
#define SRC_INCLUDE_DIALOGFORM_H_

#include "Course.h"

#include <wx/artprov.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/choice.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/spinctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>

namespace gdev {
namespace spSchedule {

class DialogForm:
		public wxDialog
{
	public:
		DialogForm( wxWindow* parent );
		virtual ~DialogForm();

		bool show( Course::Ptr& course );

		void refresh();

	protected:
		class WrapperCourse
		{
			public:
				typedef Poco::SharedPtr < WrapperCourse > Ptr;
				WrapperCourse( Course::Ptr& course ):
					mCourse( course )
				{}

				Course::Ptr& mCourse;
		};

		void onBtnCancel( wxCommandEvent& event );
		void onBtnOK( wxCommandEvent& event );

		void populate( Course::Ptr& course );
		void clean();

		Settings::Ptr mSettings;
		WrapperCourse::Ptr mWrapperCourse;

		wxTextCtrl* mCtrlEditSubject;
		wxTextCtrl* mCtrlEditTutor;
		wxTextCtrl* mCtrlEditPlace;

		wxChoice* mCtrlCBType;
		wxChoice* mCtrlCBDay;
		wxChoice* mCtrlCBWeekCycle;

		wxSpinCtrl* mCtrlSpinBeginH;
		wxSpinCtrl* mCtrlSpinBeginM;

		wxSpinCtrl* mCtrlSpinEndH;
		wxSpinCtrl* mCtrlSpinEndM;

		wxButton* mBtnOK;
		wxButton* mBtnCancel;
};

}
}

#endif
