/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_COURSE_H_
#define SRC_INCLUDE_COURSE_H_

#include "Settings.h"

#include <Poco/SharedPtr.h>
#include <Poco/Tuple.h>
#include <Poco/JSON/Object.h>

#include <string>

namespace gdev {
namespace spSchedule {

class Course
{
	public:
		typedef Poco::SharedPtr < Course > Ptr;

		Course(
				std::string subject,
				std::string tutor,
				std::string place,
				Settings::CourseType type,
				Settings::DayOfWeek day,
				Settings::WeekCycle weekCycle,
				int startHour,
				int startMinute,
				int endHour,
				int endMinute
		);
		virtual ~Course();

		static Poco::SharedPtr < std::vector < Course::Ptr > > getCourses();

		Course::Ptr clone();

		static Course::Ptr fromJSON( Poco::JSON::Object::Ptr json );
		Poco::JSON::Object::Ptr toJSON() const;

		std::string getSubject() const;
		std::string getTutor() const;
		std::string getPlace() const;
		Settings::CourseType getType() const;
		Settings::DayOfWeek getDay() const;
		Settings::WeekCycle getWeekCycle() const;
		Poco::Tuple < int, int, int, int > getTime() const;
		std::string getTimeStr() const;

		void setSubject( std::string subject );
		void setTutor( std::string tutor );
		void setPlace( std::string place );
		void setType( Settings::CourseType type );
		void setDay( Settings::DayOfWeek day );
		void setWeekCycle( Settings::WeekCycle weekCycle );
		void setTime( int startHour, int startMinute, int endHour, int endMinute );

		bool isVisible() const;

	protected:
		void verifyTime( int startHour, int startMinute, int endHour, int endMinute ) const;

		Settings::Ptr mSettings;

		static Poco::SharedPtr < std::vector < Course::Ptr > > mCourses;

		std::string mSubject;
		std::string mTutor;
		std::string mPlace;
		Settings::CourseType mType;
		Settings::DayOfWeek mDay;
		Settings::WeekCycle mWeekCycle;
		int mStartHour;
		int mStartMinute;
		int mEndHour;
		int mEndMinute;
};

}
}

#endif
