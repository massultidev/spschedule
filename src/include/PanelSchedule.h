/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_PANELSCHEDULE_H_
#define SRC_INCLUDE_PANELSCHEDULE_H_

#include "Course.h"
#include "DialogForm.h"
#include "SchedulePainter.h"

#include <wx/panel.h>

#include <vector>

namespace gdev {
namespace spSchedule {

class PanelSchedule:
		public wxPanel
{
	public:
		PanelSchedule(
				wxWindow* parent,
				DialogForm* dlgForm
		);
		virtual ~PanelSchedule();

		void refresh();

	private:
		void onPaint( wxPaintEvent& );
		void onLMB( wxMouseEvent& );
		void onRMB( wxMouseEvent& );

		Settings::Ptr mSettings;
		Poco::SharedPtr < std::vector < Course::Ptr > > mCourses;
		SchedulePainter::Ptr mSchedulePainter;

		DialogForm* mDlgForm;
};

}
}

#endif
