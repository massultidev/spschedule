/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_SCHEDULEPARSER_H_
#define SRC_INCLUDE_SCHEDULEPARSER_H_

#include "Course.h"

#include "gdev/iCal/VEvent.h"

namespace gdev {
namespace spSchedule {

class ScheduleParser
{
    public:
		static std::vector < Course::Ptr > parseICalEvents( std::vector < gdev::iCal::VEvent::Ptr > iCalEvents );

    protected:
		ScheduleParser();

		static bool sortPair(
				const std::pair < std::string, std::size_t >& left,
				const std::pair < std::string, std::size_t >& right
		);
};

}
}

#endif
