/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_SCHEDULEPAINTERPARAMETERS_H_
#define SRC_INCLUDE_SCHEDULEPAINTERPARAMETERS_H_

#include "Settings.h"

#include <wx/gdicmn.h>

namespace gdev {
namespace spSchedule {

class SchedulePainterParameters
{
	public:
		typedef Poco::SharedPtr < SchedulePainterParameters > Ptr;

		SchedulePainterParameters(
				wxSize size,
				bool isImagePainter
		);
		virtual ~SchedulePainterParameters();

		int getWidth() const;
		int getHeight() const;

		int getScaleFactor() const;

		int getOffsetFrameX() const;
		int getOffsetFrameY() const;

		int getBoardXPos() const;
		int getBoardYPos() const;
		int getBoardWidth() const;
		int getBoardHeight() const;

		int getHourCount() const;
		int getHourHeight() const;
		int getDayWidth() const;

	protected:
		Settings::Ptr mSettings;

		int mWidth;
		int mHeight;

		int mScaleFactor;

		int mOffsetFrameX;
		int mOffsetFrameY;

		int mBoardXPos;
		int mBoardYPos;
		int mBoardWidth;
		int mBoardHeight;

		int mHourCount;
		int mHourHeight;
		int mDayWidth;
};

}
}

#endif
