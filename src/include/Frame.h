/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_MAINFRAME_H_
#define SRC_INCLUDE_MAINFRAME_H_

#include "Settings.h"
#include "SchedulePainter.h"
#include "PanelSchedule.h"
#include "DialogAbout.h"
#include "DialogForm.h"
#include "DialogSettings.h"
#include "DialogResolution.h"

#include "gdev/guiWX/DialogProgress.h"
#include "gdev/utility/Loggable.h"

#include <wx/frame.h>
#include <wx/stattext.h>

#include <Poco/SharedPtr.h>

namespace gdev {
namespace spSchedule {

class Frame:
		public wxFrame,
		public gdev::utility::Loggable
{
	public:
		Frame();
		virtual ~Frame();

		bool processTask(
				gdev::guiWx::DialogProgress < Frame >& dlgProg,
				Poco::Dynamic::Var dVar,
				std::size_t caseIdx
		);

	private:
		void onClose( wxCloseEvent& );
		void onNotify( wxThreadEvent& );

		void onAdd( wxCommandEvent& );
		void onSave( wxCommandEvent& );
		void onLoad( wxCommandEvent& );
		void onImport( wxCommandEvent& );
		void onExport( wxCommandEvent& );
		void onClear( wxCommandEvent& );
		void onSettings( wxCommandEvent& );
		void onExit( wxCommandEvent& );

		void onAbout( wxCommandEvent& );

		void onLanguage( wxCommandEvent& );

		void initMenu();
		void initCtrl();

		Settings::Ptr mSettings;
		Poco::SharedPtr < std::vector < Course::Ptr > > mCourses;

		DialogAbout* mDlgAbout;
		DialogForm* mDlgForm;
		DialogSettings* mDlgSettings;
		DialogResolution* mDlgResolution;

		PanelSchedule* mPanelSchedule;
		wxStaticText* mCtrlStatus;

		enum MenuItemID {
			ID_ADD,
			ID_SAVE,
			ID_LOAD,
			ID_IMPORT,
			ID_EXPORT,
			ID_CLEAR,
			ID_SETTINGS,
			ID_EXIT,
			ID_ABOUT,
			IDS_COUNT
		};
		enum ImageFormat {
			PNG,
			JPG,
			BMP
		};
};

}
}

#endif
