/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_DIALOGABOUT_H_
#define SRC_INCLUDE_DIALOGABOUT_H_

#include <wx/dialog.h>
#include <wx/event.h>
#include <wx/sizer.h>
#include <wx/statbmp.h>
#include <wx/stattext.h>
#include <wx/button.h>

namespace gdev {
namespace spSchedule {

class DialogAbout:
		public wxDialog
{
	public:
		DialogAbout(wxWindow* parent);
		virtual ~DialogAbout();

		void show();

	protected:
		void onBtnOk(wxCommandEvent&);

		wxButton* mCtrlBtnOk;
};

}
}

#endif
