/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_SETTINGS_H_
#define SRC_INCLUDE_SETTINGS_H_

#include "gdev/guiWX/POCOwxWidgetsFix.h"

#include <wx/colour.h>

#include <Poco/SharedPtr.h>
#include <Poco/JSON/Object.h>

#include <map>
#include <string>

namespace gdev {
namespace spSchedule {

class Settings
{
	public:
		typedef Poco::SharedPtr < Settings > Ptr;

		enum CourseType {
			LECTURE,
			LABORATORY,
			EXERCISES,
			PROJECT,
			SEMINAR,
			OTHER
		};
		enum WeekCycle {
			BOTH,
			ODD,
			EVEN
		};
		enum DayOfWeek {
			MONDAY,
			TUESDAY,
			WEDNESDAY,
			THURSDAY,
			FRIDAY,
			SATURDAY,
			SUNDAY
		};

		virtual ~Settings();

		static Settings::Ptr getInstance();

		void load();
		void save();
		void reset();

		int getMinDuration() const;
		std::pair < int, int > getRangeHours() const;

		int getEdgesRadius() const;
		std::pair < int, int > getBoundsEdgesRadius() const;

		std::map < CourseType, wxColour > getCourseTypesColours() const;
		wxColour getCourseTypeColour( CourseType courseTypeID ) const;

		std::map < DayOfWeek, bool > getDaysVisibility() const;
		bool isDayVisible( DayOfWeek day ) const;
		std::size_t visibleDaysCount() const;

		std::map < CourseType, std::string > getCourseTypeLabels() const;
		std::map < std::string, CourseType > getCourseTypeIDs() const;
		std::vector < std::string > getCourseTypeLabelsVector() const;
		std::map < DayOfWeek, std::string > getDaysLabels() const;
		std::map < std::string, DayOfWeek > getDaysIDs() const;
		std::vector < std::string > getDaysLabelsVector() const;
		std::map < WeekCycle, std::string > getWeekCycleLabels() const;
		std::map < std::string, WeekCycle > getWeekCycleIDs() const;
		std::vector < std::string > getWeekCycleLabelsVector() const;

		void setEdgesRadius( int radius );
		void setRangeHour( std::pair < int, int > range );
		void setCourseTypeColour( CourseType courseTypeID, wxColour color );
		void setDayVisibility( DayOfWeek day, bool state );

	protected:
		Settings();

		void clear();
		std::string getFilePathSettings() const;
		bool doSettingsFileExist() const;
		void readFromFile();
		void saveToFile();

		void setFromJSON(Poco::JSON::Object::Ptr json);
		Poco::JSON::Object::Ptr toJSON() const;

		static Settings::Ptr mInstance;

		int mMinDuration;
		int mEdgesRadius;
		std::pair < int, int > mBoundsEdgesRadius;
		std::pair < int, int > mRangeHours;
		mutable std::map < CourseType, std::pair < std::string, wxColour > > mCourseTypes;
		mutable std::map < DayOfWeek, std::pair < std::string, bool > > mDays;
		mutable std::map < WeekCycle, std::string > mWeekCycles;
};

}
}

#endif
